// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function buildmymodule()

    mode(-1);
    lines(0);

    // Uncomment this line to make a debug version of the Toolbox
    //setenv("DEBUG_SCILAB_DYNAMIC_LINK","YES")

    // Check Scilab's version
    // ====================================================================	
    try
        v = getversion("scilab");
    catch
        error(gettext("Scilab 6.0 or more is required."));
    end

    TOOLBOX_NAME  = "spilu";
    TOOLBOX_TITLE = "Spilu";

    toolbox_path = get_absolute_file_path("builder.sce");
    tbx_builder_macros(toolbox_path);
    tbx_builder_src(toolbox_path);
    tbx_builder_gateway(toolbox_path);
    tbx_builder_help(toolbox_path);
    tbx_build_loader(TOOLBOX_NAME,toolbox_path);
    tbx_build_cleaner(TOOLBOX_NAME,toolbox_path);

endfunction

buildmymodule();
clear buildmymodule;



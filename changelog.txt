changelog of the Spilu Scilab Toolbox

spilu (v0.1.2)
    * Update for Scilab v5.4

spilu (v0.1)
    * builder.sce added in main folder
    * builder_gateway.sce added in sci_gateway folder
    * builder_gateway_c.sce added in sci_gateway/c folder
    * spilu.start and spilu.quit added in etc folder
    * builder_src.sce and cleaner_src.sce added in src folder
    * builder_fortran.sce added in src/fortran folder
    * Converted old man pages to new docbook format.
    * Manually formatted the help pages.
    * Created spilu_getpath.
    * Fixed bugs in the help pages.
    * Added comments to permVecToMat.
    * Renamed permVecToMat to spilu_permVecToMat.
    * Added returns after calls to Scierror in the gateway.
    * Added a test for error cases of spilu_ilu0.
    * Added export header in conv.h.
    * Added spilu_ilu0M function for comparison.
    * Fixed bug #493: spilu_ilu0 failed on Windows.
      The free of the ia array is in the gateway's dll.
      Therefore, the malloc of the ia array must also be 
      in the gateway's dll.
    * Fixed bug #535: The spilu_ilu0 function did not 
      produce the correct (L,U) pair.
      The problem was an unconsistent declaration of tl in ilu0, which 
      is a double and not an integer.
      The problem was created by the commit 81, which 
      removed implicit declarations.
    * Removed "real*8" declarations in the Fortran, used 
      "double precision" instead.
      Removed implicit declarations in the Fortran.
    * Refactoring of the gateways.
    * Any optional argument equal to the empty matrix [] is 
      replaced by its default value.
    * Filled example for iluk.
    * Fixed bug (crash) in iluk, when elts parameter is too small.
      The error is "not enough memory".
    * Created spilu_decomprand, spilu_iludplot, 
      spilu_plotparameter, spilu_spspdrand to make testing easier.
    * Created spilu_spluget to manage internal matrix format.
    * Fixed bug #570.
      ilud and iludp sometimes produced "not enough memory".
    * Created iludpplot for ILUDP.
    * Created bandwidthsum to clarify elts parameter of spilu_iluk.
    * Created ilukM to compare with iluk.
    * Created an overview for the toolbox.
    * Removed the elts parameter of spilu_iluk for simplicity
      and uniformity.
    * Fixed bug #445: 
      ilu0 crash when a zero pivot is encountered
      The ju array was uninitialized in ilu0 and milu0.
    * Created iluhub, iluhubavail and iluhubparname for benchmarks.
    * Created RILUT demo, adapted from Sparskit.
    * Created PDE225 demo, to see the effect of preconditionning 
      on GMRES.
    * Created West0479 demo, to see the effect of drop 
      on ILUTP.


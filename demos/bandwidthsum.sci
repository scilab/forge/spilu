// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function bd = spilu_bandwidthsum(A)
    // Returns the sum of the bandwidths of all rows.
    // 
    // Calling Sequence
    //   bd = spilu_bandwidthsum(A)
    //
    // Parameters
    //   A : a n-by-n sparse matrix of doubles
    //   bd : a 1-by-1 matrix of doubles, integer value, positive, the sum of the bandwidths of all rows
    //
    // Description
    // For each row, the bandwidth is icolMax - icolMin + 1,
    // where icolMax is the maximum column index of a nonzero entry in the 
    // row and icolMin is the minimum column index of a nonzero entry in the 
    // row.
    // This function returns the sum of the bandwiths of all rows.
	//
	// This algorithm is used internally by several functions, in order 
	// to compute the size of the temporary array required for the 
	// computations.
    //
    // Examples
    // // A 6-by-6 sparse matrix
    // // nnz(A) = 16
    // A=[
    // -1.    3.    0.    0.    4.    0.  
    // 2.   -5.    0.    0.    0.    1.  
    // 0.    0.   -2.    3.    0.    0.  
    // 0.    0.    7.   -1.    0.    0.  
    // -3.    0.    0.    4.    6.    0.
    // 0.    5.    0.    0.   -7.    8. 
    // ];
	// bd = spilu_bandwidthsum(A)
    // 
    // // Here, the value of bd is : 25 = 5+6+2+2+5+5.
    // // Indeed, for the row #1, the bandwidth is 5-1+1=5 and 
    // // for the row #3, the bandwidth is 4-3+1=2.
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
	
	[lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_bandwidthsum" , rhs , 1 )
    apifun_checklhs ( "spilu_bandwidthsum" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "spilu_bandwidthsum" , A , "A" , 1 , "sparse" )
    //
    // Check size
    // Nothing to do
	//
	// Proceed...

    bd=0

    n = size(A,"r")
    for i=1 : n
        ij = spget(A(i,:))
        bd = bd + ij($,2) - ij(1,2) + 1
    end
endfunction


// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->


function decompranddemo()

    path  = get_absolute_file_path("decomprand.sce");
    exec(fullfile(path,"decomprand.sci"));
    exec(fullfile(path,"spspdrand.sci"));

    mprintf("Testing decomprand\n");
    mprintf("See demos/decomprand.sci for details.\n");

    function tf=isIludSensitive(A)
        tf = %f
        //
        // Search initial decomposition
        instr = "[L0,U0]=spilu_ilud(A)";
        ierr = execstr(instr,"errcatch");
        if ( ierr <> 0 ) then
            return
        end
        //
        // See if alpha is sensitive
        [L,U]=spilu_ilud(A,1.);
        tfL = and(L0==L);
        tfU = and(U0==U);
        if ( tfL | tfU ) then
            return
        end
        //
        // See if drop is sensitive
        [L,U]=spilu_ilud(A,[],1.);
        tfL = and(L0==L);
        tfU = and(U0==U);
        if ( ~tfL & ~tfU ) then
            tf = %t
        end
    endfunction
    // Find a 5-by-5 sparse matrix, with integer entries in [-10,10],
    // such that ilud is sensitive to the alpha and drop parameters.
    // Perform 100 trials.
	rand("seed",0)
	grand("setsd",0)
    [A,k] = spilu_decomprand(5,list(isIludSensitive),..
    list(spilu_spspdrand,-10,10,2.),100);
    assert_checkequal(size(A),[5 5]);
    assert_checktrue(k>1);

    //
    // Load this script into the editor
    //
    filename = "decomprand.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
    editor ( fullfile(dname,"decomprand.sci") );
endfunction 
decompranddemo();
clear decompranddemo



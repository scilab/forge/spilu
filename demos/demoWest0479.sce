// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <--JVM NOT MANDATORY -->

// Bibliography
// WEST0479: Chemical engineering plant models
// Eight stage column section, all rigorous
// from set CHEMWEST, from the Harwell-Boeing Collection
// http://math.nist.gov/MatrixMarket/data/Harwell-Boeing/chemwest/west0479.html

function west0479Demo()

    mprintf("See the LU factorizations in action on West 0479 matrix.\n")
    path = spilu_getpath (  );
    filename = fullfile(path,"tests","matrices","west0479.mtx");
    A=mmread(filename);
    nrows = size(A,"r");
    ncols = size(A,"c");
    mprintf("Matrix Sparse : %d-by-%d\n",nrows,ncols);
    mprintf("nnz(A)=%d\n",nnz(A));
    //
    // Plot A
    scf();
    PlotSparse(A);
    xtitle("Sparsity pattern of West0479");
    //
    // Plot full LU decomposition
    [L,U]=lu(full(A));
    LU = full(L)+full(U);
    scf();
    PlotSparse(sparse(LU));
    xtitle("Sparsity pattern of LU(West0479)");
    //
    // Compare various values of drop for ILUTP
    scf();
    //
    [L,U,perm]=spilu_ilutp(A,[],0.);
    subplot(2,2,1);
    PlotSparse(L*U);
    xtitle("Sparsity pattern of ILUTP(drop=0.)");
    //
    [L,U,perm]=spilu_ilutp(A,[],0.1);
    subplot(2,2,2);
    PlotSparse(L*U);
    xtitle("Sparsity pattern of ILUTP(drop=0.1)");
    //
    subplot(2,2,3);
    [L,U,perm]=spilu_ilutp(A,[],1);
    PlotSparse(L*U);
    xtitle("Sparsity pattern of ILUTP(drop=1)");
    //
    [L,U,perm]=spilu_ilutp(A,[],10);
    subplot(2,2,4);
    PlotSparse(L*U);
    xtitle("Sparsity pattern of ILUTP(drop=10)");
    //
    // Plot sensitivity with respect to drop
    [data,hAlpha,hDrop,hPermtol,hBloc] = spilu_ilutpplot(A);
    delete(hAlpha);
    delete(hPermtol);
    delete(hBloc);
    hDrop.children(2).log_flags = "lln";

    //
    // Load this script into the editor
    //
    filename = "demoWest0479.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
endfunction 
west0479Demo();
clear west0479Demo;

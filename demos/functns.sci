// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// Copyright (C) 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// This script is a Scilab port of SPARSKIT2/MATGEN/FDIF/functns.f

//-----------------------------------------------------------------------
//     contains the functions needed for defining the PDE problems. 
//
//     first for the scalar 5-point and 7-point PDE 
//-----------------------------------------------------------------------
function output = afun (x,y,z)
    output = -1.
endfunction

function output = bfun (x,y,z)
    output = -1.
endfunction

function output = cfun (x,y,z)
    output = -1.
endfunction

function output = dfun (x,y,z)
    //     data gamma /100.0/ 
    //     dfun = gamma * exp( x * y )
    output = 10.d0
endfunction

function output = efun (x,y,z)
    //      data gamma /100.0/ 
    //     efun = gamma * exp( (- x) * y ) 
    output = 0.d0
endfunction

function output = ffun (x,y,z)
    output = 0.0
endfunction

function output = gfun (x,y,z)
    output = 0.0 
endfunction

function output = hfun(x, y, z)
    output = 0.0
endfunction

function output = betfun(side, x, y, z)
    output = 1.0
endfunction

function output = gamfun(side, x, y, z)
    if (side=='x2') then
        output = 5.0
    elseif (side=='y1') then
        output = 2.0
    elseif (side=='y2') then
        output = 7.0
    else
        output = 0.0
    end
endfunction

//-----------------------------------------------------------------------
//     functions for the block PDE's 
//-----------------------------------------------------------------------
function coeffs = afunbl (nfree,x,y,z)
    for j=1:nfree
        for i=1:nfree
            coeffs((j-1)*nfree+i) = 0.
        end
        coeffs((j-1)*nfree+j) = -1.
    end
endfunction

function coeffs = bfunbl (nfree,x,y,z)
    for j=1:nfree
        for i=1:nfree
            coeffs((j-1)*nfree+i) = 0.
        end
        coeffs((j-1)*nfree+j) = -1.
    end
endfunction

function coeffs = cfunbl (nfree,x,y,z)
    for j=1:nfree
        for i=1:nfree
            coeffs((j-1)*nfree+i) = 0.
        end
        coeffs((j-1)*nfree+j) = -1.
    end
endfunction

function coeffs = dfunbl (nfree,x,y,z)
    for j=1:nfree
        for i=1:nfree
            coeffs((j-1)*nfree+i) = 0.
        end
    end
endfunction

function coeffs = efunbl (nfree,x,y,z)
    for j=1:nfree
        for i=1:nfree
            coeffs((j-1)*nfree+i) = 0.
        end
    end
endfunction

function coeffs = ffunbl (nfree,x,y,z)
    for j=1:nfree
        for i=1:nfree
            coeffs((j-1)*nfree+i) = 0.
        end
    end
endfunction

function coeffs = gfunbl (nfree,x,y,z)
    for j=1:nfree
        for i=1:nfree
            coeffs((j-1)*nfree+i) = 0.
        end
    end
endfunction
//-----------------------------------------------------------------------
//     The material property function xyk for the 
//     finite element problem 
//-----------------------------------------------------------------------
function xyke = xyk(nel,x,y,ijk,node)
    //     
    //     this is the identity matrix.
    //     
    xyke(1,1) = 1.
    xyke(2,2) = 1.
    xyke(1,2) = 0.
    xyke(2,1) = 0.

endfunction

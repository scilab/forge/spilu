// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) Yousef Saad
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function ilu0Fortrandemo()

    path  = get_absolute_file_path("ilu0Fortran.sce");
    exec(fullfile(path,"ilu0Fortran.sci"));
    exec(fullfile(path,"spluget.sci"));

    mprintf("Testing ilu0Fortran\n")
    mprintf("See demos/ilu0Fortran.sci for details.\n")

    //
    // Simplest possible test 3-by-3 test case
    // nnz = 7
    A = [
    1 2 0
    3 4 5
    0 6 7
    ];
    A = sparse(A);
    n = size(A,"r");
    // Convert into Compressed Row Storage
    [row_ptr,col_ind,val]=sp2adj(A')
    // Factor into ilu0
    [alu, jlu, ju, iw, ierr] = ilu0Fortran(val, col_ind, row_ptr)
    //
    alu_expected=[
    1.
    -0.5
    0.045454545454545456
    0.
    2.
    3.
    5.
    -3.
    ]
    jlu_expected=[
    5
    6
    8
    9
    2
    1
    3
    2
    ]
    ju_expected=[
    5
    7
    9
    ]
    //
    // Convert the L and U factors from MSR into CSR
    [Lrow_ptr, Lcol_ind, Lnnz, Lval, ..
    Urow_ptr, Ucol_ind, Unnz, Uval]= spilu_spluget(n,ju,jlu,alu)
    //
    Lnnz_expected = 5
    Lval_expected  = [ 
    1.  
    3.  
    1.  
    -3.  
    1.  
    ]
    Lcol_ind_expected  = [
    1.  
    1.  
    2.  
    2.  
    3.  
    ]
    Lrow_ptr_expected  = [
    1.  
    2.  
    4.  
    6.  
    ]
    Unnz_expected = 5
    Uval_expected  = [
    1.   
    2.   
    - 2.   
    5.   
    22.  
    ]
    Ucol_ind_expected  = [
    1.  
    2.  
    2.  
    3.  
    3.  
    ]
    Urow_ptr_expected  = [
    1.  
    3.  
    5.  
    6.  
    ]

    // Convert L, U from CRS to sparse
    L=adj2sp(Lrow_ptr,Lcol_ind,Lval)'
    U=adj2sp(Urow_ptr,Ucol_ind,Uval)'
    //
    // Expected values
    Le = [
    1  0 0
    3  1 0
    0 -3 1
    ];
    Le = sparse(Le);
    Ue = [
    1  2 0
    0 -2 5
    0  0 22
    ];
    Ue = sparse(Ue);
    assert_checkequal(full(L),full(Le));
    assert_checkequal(full(U),full(Ue));
    //
    // A 6-by-6 test case
    // nnz = 16
    A=[
    -1.   3.    0.    0.    4.    0.  
    2.   -5.    0.    0.    0.    1.  
    0.    0.   -2.    3.    0.    0.  
    0.    0.    7.   -1.    0.    0.  
    -3.   0.    0.    4.    6.    0.
    0.    5.    0.    0.   -7.    8.  
    ];        
    A=sparse(A);
    n = size(A,"r");
    // Convert into Compressed Row Storage
    [row_ptr,col_ind,val]=sp2adj(A')
    // Factor into ilu0
    [alu, jlu, ju, iw, ierr] = ilu0Fortran(val, col_ind, row_ptr)
    // Convert the L and U factors from MSR into CSR
    [Lrow_ptr, Lcol_ind, Lnnz, Lval, ...
    Urow_ptr, Ucol_ind, Unnz, Uval]= spilu_spluget(n,ju,jlu,alu)
    // Convert L, U from CRS to sparse
    L=adj2sp(Lrow_ptr,Lcol_ind,Lval)'
    U=adj2sp(Urow_ptr,Ucol_ind,Uval)'
    //
    expL=[
    1.    0.    0.     0.           0.           0.  
    -2.   1.    0.     0.           0.           0.  
    0.    0.    1.     0.           0.           0.  
    0.    0.   -3.5    1.           0.           0.  
    3.    0.    0.     0.4210526    1.           0.  
    0.    5.    0.     0.           1.1666667    1.  
    ];
    expU=[
    -1.    3.    0.    0.     4.    0.  
    0.    1.    0.    0.     0.    1.  
    0.    0.   -2.    3.     0.    0.  
    0.    0.    0.    9.5    0.    0.  
    0.    0.    0.    0.    -6.    0.  
    0.    0.    0.    0.     0.    3.  
    ];

    L=full(L);
    U=full(U);
    assert_checkalmostequal(L,expL,0,1D-7,"element");
    assert_checkalmostequal(U,expU,0,1D-7,"element");

    //
    // Load this script into the editor
    //
    filename = "ilu0Fortran.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
    editor ( fullfile(dname,"ilu0Fortran.sci") );
endfunction 
ilu0Fortrandemo();
clear ilu0Fortrandemo;


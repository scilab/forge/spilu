// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// Search for a matrix A for which ILUD is sensitive to the 
// alpha and drop parameters.

function iludSearchDemo()

    mprintf("Search for a matrix A for which ILUD is sensitive to the \n")
    mprintf("alpha and drop parameters.\n")

    path  = get_absolute_file_path("ilud_searchForMatrix.sce");
    exec(fullfile(path,"decomprand.sci"));
    exec(fullfile(path,"spspdrand.sci"));

    function tf = isIludSensitive(A)
        tf = %f
        //
        // Search initial decomposition
        instr = "[L0,U0]=spilu_ilud(A)";
        ierr = execstr(instr,"errcatch");
        if ( ierr <> 0 ) then
            return
        end
        //
        // See if alpha is sensitive
        [L,U]=spilu_ilud(A,1.);
        tfL = and(L0==L);
        tfU = and(U0==U);
        if ( tfL & tfU ) then
            // Matrix A is not sensitive to alpha
            return
        end
        //
        // See if drop is sensitive
        [L,U]=spilu_ilud(A,[],1.);
        tfL = and(L0==L);
        tfU = and(U0==U);
        if ( tfL & tfU ) then
            // Matrix A is not sensitive to drop
            return
        end
        // Therefore, all options are sensitive
        tf = %t
    endfunction

    [A,k] = spilu_decomprand(5,list(isIludSensitive),..
    list(spilu_spspdrand,-10,10,2.),100);
    mprintf("Matrix Found in %d trials.\n",k)
    mprintf("A=\n")
    disp(full(A))
    spilu_iludplot(A,100);

    //
    // Load this script into the editor
    //
    filename = "ilud_searchForMatrix.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
endfunction 
iludSearchDemo();
clear iludSearchDemo;



// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// Search for a matrix A for which ILUDP is sensitive to the 
// alpha and drop parameters.

function iludpSearchDemo()

    mprintf("Search for a matrix A for which ILUDP is sensitive to the \n")
    mprintf("alpha and drop parameters.\n")

    path  = get_absolute_file_path("iludp_searchForMatrix.sce");
    exec(fullfile(path,"decomprand.sci"));
    exec(fullfile(path,"spspdrand.sci"));

    function tf = isIludpSensitive(A)
        tf = %f
        //
        // Search initial decomposition
        instr = "[L0,U0,perm0]=spilu_iludp(A)";
        ierr = execstr(instr,"errcatch");
        if ( ierr <> 0 ) then
            return
        end
        //
        // See if alpha is sensitive
        [L,U,perm]=spilu_iludp(A,1.);
        tfL = and(L0==L);
        tfU = and(U0==U);
        tfP = and(perm0==perm);
        if ( tfL & tfU & tfP ) then
            // Matrix A is not sensitive to alpha
            return
        end
        //
        // See if drop is sensitive
        [L,U,perm]=spilu_iludp(A,[],1.);
        tfL = and(L0==L);
        tfU = and(U0==U);
        tfP = and(perm0==perm);
        if ( tfL & tfU & tfP ) then
            // Matrix A is not sensitive to drop
            return
        end
        //
        // See if permtol is sensitive
        [L,U,perm]=spilu_iludp(A,[],[],0.);
        tfL = and(L0==L);
        tfU = and(U0==U);
        tfP = and(perm0==perm);
        if ( tfL & tfU & tfP ) then
            // Matrix A is not sensitive to permtol
            return
        end
        //
        // See if bloc is sensitive
        [L,U,perm]=spilu_iludp(A,[],[],[],1);
        tfL = and(L0==L);
        tfU = and(U0==U);
        tfP = and(perm0==perm);
        if ( tfL & tfU & tfP ) then
            return
        end
        // Therefore, all options are sensitive
        tf = %t
    endfunction

    // Generate a matrix which is sparse and symmetric.
    function A = myspdrand(n,a,b)
        A = spilu_spspdrand(n,a,b,0.)
        A = A + speye(n,n)
    endfunction

    [A,k] = spilu_decomprand(5,list(isIludpSensitive),..
    list(myspdrand,-10,10),1000);
    mprintf("Matrix Found in %d trials.\n",k)
    mprintf("A=\n")
    disp(full(A))
    spilu_iludpplot(A,100);

    //
    // Load this script into the editor
    //
    filename = "iludp_searchForMatrix.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
endfunction
iludpSearchDemo();
clear iludpSearchDemo;

// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// Search for a matrix A for which ILUT is sensitive to the 
// lfil and drop parameters.

function ilutSearchDemo()

    mprintf("Search for a matrix A for which ILUT is sensitive to the \n")
    mprintf("lfil and drop parameters.\n")
    mprintf("It may be required to run the demo several times \n")
    mprintf("before the algorithm succeeds.\n")

    path  = get_absolute_file_path("ilut_searchForMatrix.sce");
    exec(fullfile(path,"decomprand.sci"));
    exec(fullfile(path,"spspdrand.sci"));

    function tf = isIlutSensitive(A)
        tf = %f
        //
        // Search initial decomposition
        instr = "[L0,U0]=spilu_ilut(A)";
        ierr = execstr(instr,"errcatch");
        if ( ierr <> 0 ) then
            return
        end
        //
        // See if lfil is sensitive
        n = size(A,"r")
        [L,U]=spilu_ilut(A,n);
        tfL = and(L0==L);
        tfU = and(U0==U);
        if ( tfL & tfU ) then
            // Matrix A is not sensitive to lfil
            return
        end
        //
        // See if drop is sensitive
        [L,U]=spilu_ilut(A,[],0.);
        tfL = and(L0==L);
        tfU = and(U0==U);
        if ( tfL & tfU ) then
            // Matrix A is not sensitive to drop
            return
        end
        // Therefore, all options are sensitive
        tf = %t
    endfunction


    [A,k] = spilu_decomprand(5,list(isIlutSensitive),..
    list(spilu_spspdrand,-10,10,1.),1000);
    mprintf("Matrix Found in %d trials.\n",k)
    mprintf("nnz(A)=%d\n",nnz(A))
    spilu_ilutplot(A,100);

    //
    // Load this script into the editor
    //
    filename = "ilut_searchForMatrix.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );
endfunction 
ilutSearchDemo();
clear ilutSearchDemo;

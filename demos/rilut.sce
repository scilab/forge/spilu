// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// Copyright (C) 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <--JVM NOT MANDATORY -->

// This script is a Scilab port of SPARSEKIT2/ITSOL/rilut.f

//-----------------------------------------------------------------------
//     Test program for ilut preconditioned gmres.
//     This program generates a sparse matrix using
//     matgen and then solves a linear system with an 
//     artificial rhs.
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
//     pde to be discretized is :
//---------------------------
//     
//     -Lap u + gammax exp (xy)delx u + gammay exp (-xy) dely u +alpha u
//     
//     where Lap = 2-D laplacean, delx = part. der. wrt x,
//     dely = part. der. wrt y.
//     gammax, gammay, and alpha are passed via the commun func.
//     
//-----------------------------------------------------------------------

function rilutdemo()

    path  = get_absolute_file_path("rilut.sce");
    exec(fullfile(path,"genmat.sci"));
    exec(fullfile(path,"functns.sci"));

    //     
    //     data for PDE:
    //     
    nx = 30; 
    ny = 30 ;
    nz = 1;
    alpha = -50.0;
    gammax = 10.0;
    gammay = 10.0;
    //     
    //     data for GMRES
    //
    nmax = 5000;
    im   = 10;
    eps  = 1.0D-07;
    maxits = 100 ;
    iout = 6;
    permtol = 1.0;
    ipar(2) = 2;
    ipar(3) = 2;
    ipar(4) = 20*nmax;
    ipar(5) = im;
    ipar(6) = maxits;
    fpar(1) = eps;
    fpar(2) = 2.22D-16;
    //     
    //     same initial guess for gmres 
    //     
    //--------------------------------------------------------------
    //     call gen57 to generate matrix in compressed sparse row format
    //--------------------------------------------------------------
    //     
    //     define part of the boundary condition here
    //     
    al(1) = 0.0;
    al(2) = 1.0;
    al(3) = 0.0;
    al(4) = 0.0;
    al(5) = 0.0;
    al(6) = 0.0;
    mmode = 0;
    [n,a,ja,ia,iau,rhs] = gen57pt(nx,ny,nz,al,mmode);
    // Convert L, U from CRS to sparse
    A=adj2sp(ia,ja,a)';
    //
    //     zero initial guess to the iterative solvers
    //
    xran = zeros(n,1);
    mprintf("RILUT:  generated a finite difference matrix\n")
    mprintf("        grid size = %d-by-%d-by-%d\n", nx, ny, nz)
    mprintf("        matrix size = %d\n", n)
    //--------------------------------------------------------------
    //     generate right hand side = A * (1,1,1,...,1)**T
    //--------------------------------------------------------------
    x = ones(nx*ny*nz,1);
    y = A*x;
    //--------------------------------------------------------------
    //     test all different methods available:
    //     ILU0, MILU0, ILUT and with different values of tol and lfil
    //     ( from cheaper to more expensive preconditioners)
    //     The more accurate the preconditioner the fewer iterations 
    //     are required in pgmres, in general. 
    //     
    //--------------------------------------------------------------


    for k = 1 : 16
        select k
        case 1 then
            m = "ilu0";
            args = list();
        case 2 then
            m = "milu0";
            args = list();
        case 3 then
            m = "ilut";
            tol  = 0.0001;
            lfil = 5;
            args = list(lfil,tol);
        case 4 then
            m = "ilut";
            tol = 0.0001;
            lfil = 10;
            args = list(lfil,tol);
        case 5 then
            m = "ilut";
            tol = 0.0001;
            lfil = 15;
            args = list(lfil,tol);
        case 6 then
            m = "ilutp";
            tol  = 0.0001;
            lfil = 5;
            args = list(lfil,tol,permtol);
        case 7 then
            m = "ilutp";
            tol = 0.0001;
            lfil = 10;
            args = list(lfil,tol,permtol);
        case 8 then
            m = "ilutp";
            tol = 0.0001;
            lfil = 15;
            args = list(lfil,tol,permtol);
        case 9 then
            m = "iluk";
            lfil = 0;
            args = list(lfil);
        case 10 then
            m = "iluk";
            lfil = 1;
            args = list(lfil);
        case 11 then
            m = "iluk";
            lfil = 3 ;
            args = list(lfil);
        case 12 then
            m = "iluk";
            lfil = 6;
            args = list(lfil);
        case 13 then
            m = "ilud";
            tol = 0.075 ;
            alph= 0.0 ;
            args = list(alph,tol);
        case 14 then
            m = "ilud";
            tol = 0.075 ;
            alph=1.0 ;
            args = list(alph,tol);
        case 15 then
            m = "ilud";
            tol = 0.01;
            alph=1.0 ;
            args = list(alph,tol);
        case 16 then
            m = "iludp";
            args = list();
        else
            error("Unknown method")
        end
        mprintf("Preconditioner#%d: %s\n",k,m)
        for i = 1 : length(args)
            name = spilu_iluhubparname(m,i);
            mprintf("    %s=%s",name,string(args(i)))
        end
        if ( length(args) > 0 ) then
            mprintf("\n");
        end
        [L,U,perm]=spilu_iluhub(A,m,args(:));
        mprintf("    nnz(A) = %d, nnz(LU) = %d\n", nnz(A), nnz(L)+nnz(U))
        P = spilu_permVecToMat(perm);
        [xcomp, err, iterK, flag] = imsls_gmres( P*A, P*y, [], L, U);
        digits = assert_computedigits(x,xcomp);
        digits = min(digits);
        mprintf("    Digits in X = %d\n", digits );
        mprintf("    Final relative residual norm = %d\n", err );
        mprintf("    Iterations = %d\n", iterK );
        mprintf("    Flag = %d\n", flag );
    end

    //
    // Load this script into the editor
    //
    filename = "rilut.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );

endfunction

rilutdemo();
clear rilutdemo;

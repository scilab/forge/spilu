// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// Templates for the Solution of Algebraic Eigenvalue Problems: 
// a Practical Guide
// Edited by Zhaojun Bai, James Demmel, Jack Dongarra, 
// Axel Ruhe, and Henk van der Vorst
// "Sparse Matrix Storage Formats"
// J. Dongarra

// * sp2adj : convert from Scilab Sparse into CCS or CRS
// * adj2sp : convert from CCS or CRS to Scilab Sparse

function sparseStorageDemo()

    mprintf("How to use sp2adj and adj2sp.\n")

    // http://web.eecs.utk.edu/~dongarra/etemplates/book.html
    A = [
    10 0 0 0 -2 0
    3 9 0 0 0 3
    0 7 8 7 0 0
    3 0 8 7 5 0
    0 8 0 9 9 13
    0 4 0 0 2 -1
    ];
    A = sparse(A)

    mprintf("Converting from Scilab sparse\n")
    mprintf("into Compressed Column Storage (CCS)\n")
    // To get the Compressed Column Storage (CCS) :
    [col_ptr,row_ind,val]=sp2adj(A)
    // To convert back to sparse:
    AAsp=adj2sp(col_ptr,row_ind,val)
    // Check the conversion
    disp(AAsp - A)

    mprintf("Converting from Scilab sparse\n")
    mprintf("into Compressed Row Storage (CRS)\n")
    // To get the Compressed Row Storage (CRS) :
    [row_ptr,col_ind,val]=sp2adj(A')
    // To convert back to sparse:
    AAsp=adj2sp(row_ptr,col_ind,val)'
    // Check the conversion
    disp(AAsp - A)

    //
    // Load this script into the editor
    //
    filename = "sparseStorageFormats.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );

endfunction 
sparseStorageDemo();
clear sparseStorageDemo;

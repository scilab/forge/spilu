// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function A = spilu_spspdrand(n,a,b,alpha)
    // Create a square sparse symmetric positive definite matrix.
    // 
    // Calling Sequence
    //   A = spilu_spspdrand(n,a,b,alpha)
    //
    // Parameters
    // n: a 1-by-1 matrix of doubles, integer value, positive, the size of the matrix A.
    // a : a 1-by-1 matrix of doubles, integer value, the minimum value of the off-diagonal terms.
    // b : a 1-by-1 matrix of doubles, integer value, the maximum value of the off-diagonal terms.
    // alpha: a 1-by-1 matrix of doubles, the diagonal coefficient. Should be greater or equal to 1. If alpha>0, then A(i,i) is updated depending on sum(abs(A(i,:))). If alpha==2, then A is positive definite. If alpha==1, then alpha is positive semi-definite.
    // A: a n-by-n sparse matrix of doubles, symmetric positive definite.
    //
    // Description
    // Create a random sparse n-by-n matrix with off-diagonal entries uniform in
    // [a,b).
    // Diagonal entries are generally larger.
    // The matrix A is guaranteed to be symmetric positive definite.
    //
    // Examples
    // // Produce a 5-by-5 sparse matrix, with integer off-diagonal entries
    // // in [-10,10], symmetric positive definite.
    // A = spilu_spspdrand(5,-10,10,2.)
    // // Produce a 5-by-5 sparse matrix, with integer off-diagonal entries
    // // in [-10,10], symmetric.
    // A = spilu_spspdrand(5,-10,10,0.)
    //
    // Authors
    //   Copyright (C) 2011 - DIGITEO - Michael Baudin

    U=sprand(n,n,0.1);
    // Create a sparse matrix M with entries 
    // at the same positions as U.
    // Then put 1 into these positions.
    [ij,v,mn]=spget(U);
    v = ones(v);
    M = sparse(ij,v,mn);
    // Scale U into [a,b]
    A = U*(b - a + 1) + M*a;
    // Get integers
    A = floor(A);
    // Make the matrix symmetric.
    A = A+A';
    // Set the diagonal to make it positive definite.
    if ( alpha > 0 ) then
        for i = 1 : n
            rowi = sum(abs(A(i,:)));
            A(i,i) = max(alpha * rowi,1.);
        end
    end
endfunction

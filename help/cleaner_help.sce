// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

libpath = get_absolute_file_path('cleaner_help.sce');
exec(fullfile(libpath,"en_US","cleanhelp.sce"));

clear libpath;

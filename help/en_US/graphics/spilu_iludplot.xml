<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from spilu_iludplot.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="spilu_iludplot" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>spilu_iludplot</refname><refpurpose>Plots the sensitivity of ILUD for A.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   spilu_iludplot(A)
   data = spilu_iludplot(A)
   data = spilu_iludplot(A,N)
   [data,hAlpha] = spilu_iludplot(...)
   [data,hAlpha,hDrop] = spilu_iludplot(...)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A:</term>
      <listitem><para> a n-by-n sparse matrix</para></listitem></varlistentry>
   <varlistentry><term>N:</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, the number of points in the plot (default N = 100)</para></listitem></varlistentry>
   <varlistentry><term>data:</term>
      <listitem><para> a N-by-6 matrix of doubles, the values of the parameters</para></listitem></varlistentry>
   <varlistentry><term>hAlpha:</term>
      <listitem><para> a graphics handle, the alpha plot</para></listitem></varlistentry>
   <varlistentry><term>hDrop:</term>
      <listitem><para> a graphics handle, the drop plot</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
For one particular matrix, plot norm(A-L*U)/norm(A) and nnz(L)+nnz(U), depending on
alpha and drop for the ILUD method.
   </para>
   <para>
The columns of the array data are:
<itemizedlist>
<listitem><para>
data(:,1) : the values of alpha
</para></listitem>
<listitem><para>
data(:,2) : the values of norm(A-L*U)/norm(A) with respect to alpha
</para></listitem>
<listitem><para>
data(:,3) : the values of nnz(L)+nnz(U) with respect to alpha
</para></listitem>
<listitem><para>
data(:,4) : the values of drop
</para></listitem>
<listitem><para>
data(:,5) : the values of norm(A-L*U)/norm(A) with respect to drop
</para></listitem>
<listitem><para>
data(:,6) : the values of nnz(L)+nnz(U) with respect to drop
</para></listitem>
</itemizedlist>
   </para>
   <para>
If the decomposition does not work for one value of the parameter,
then norm(A-L*U)/norm(A) and nnz(L)+nnz(U) are set to Infinity.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A = [
34.    4.   -10.    0.  -3.
4.     24.  -8.     0.    0.
-10.  -8.     36.    0.    0.
0.     0.     0.     1.    0.
-3.     0.     0.     0.    6.
];
A = sparse(A);
spilu_iludplot(A);

// See on a 225-by-225 sparse matrix
path = spilu_getpath (  );
filename = fullfile(path,"tests","matrices","pde225.mtx");
A=mmread(filename);
[data,hLfil,hDrop] = spilu_iludplot(A);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>

<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from spilu_ilutpplot.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="spilu_ilutpplot" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>spilu_ilutpplot</refname><refpurpose>Plots the sensitivity of ILUT for A.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   spilu_ilutpplot(A)
   data = spilu_ilutpplot(A)
   data = spilu_ilutpplot(A,N)
   [data,hLfil] = spilu_ilutpplot(...)
   [data,hLfil,hDrop] = spilu_ilutpplot(...)
   [data,hAlpha,hDrop,hPermtol] = spilu_ilutpplot(...)
   [data,hAlpha,hDrop,hPermtol,hBloc] = spilu_ilutpplot(...)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A:</term>
      <listitem><para> a n-by-n sparse matrix</para></listitem></varlistentry>
   <varlistentry><term>N:</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, the number of points in the plot (default N = 100).</para></listitem></varlistentry>
   <varlistentry><term>data:</term>
      <listitem><para> a N-by-6 matrix of doubles, the values of the parameters</para></listitem></varlistentry>
   <varlistentry><term>hLfil:</term>
      <listitem><para> a graphics handle, the lfil plot</para></listitem></varlistentry>
   <varlistentry><term>hDrop:</term>
      <listitem><para> a graphics handle, the drop plot</para></listitem></varlistentry>
   <varlistentry><term>hPermtol:</term>
      <listitem><para> a graphics handle, the permtol plot</para></listitem></varlistentry>
   <varlistentry><term>hBloc:</term>
      <listitem><para> a graphics handle, the bloc plot</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
For one particular matrix, plot norm(A-L*U)/norm(A) and nnz(L)+nnz(U), depending on
lfil and drop for the ILUT method.
   </para>
   <para>
The columns of the array data are:
<itemizedlist>
<listitem><para>
data(:,1) : the values of lfil
</para></listitem>
<listitem><para>
data(:,2) : the values of norm(A-L*U)/norm(A) with respect to lfil
</para></listitem>
<listitem><para>
data(:,3) : the values of nnz(L)+nnz(U) with respect to lfil
</para></listitem>
<listitem><para>
data(:,4) : the values of drop
</para></listitem>
<listitem><para>
data(:,5) : the values of norm(A-L*U)/norm(A) with respect to drop
</para></listitem>
<listitem><para>
data(:,6) : the values of nnz(L)+nnz(U) with respect to drop
</para></listitem>
<listitem><para>
data(:,7) : the values of permtol
</para></listitem>
<listitem><para>
data(:,8) : the values of norm(P*A-L*U)/norm(A) with respect to permtol
</para></listitem>
<listitem><para>
data(:,9) : the values of nnz(L)+nnz(U) with respect to permtol
</para></listitem>
<listitem><para>
data(:,10) : the values of bloc
</para></listitem>
<listitem><para>
data(:,11) : the values of norm(P*A-L*U)/norm(A) with respect to bloc
</para></listitem>
<listitem><para>
data(:,12) : the values of nnz(L)+nnz(U) with respect to bloc
</para></listitem>
</itemizedlist>
   </para>
   <para>
If the decomposition does not work for one value of the parameter,
then norm(A-L*U)/norm(A) and nnz(L)+nnz(U) are set to Infinity.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
A = [
10.     -6.    -1.     0.     3.
-6.     34.     0.    10.     0.
-1.      0.     1.     0.     0.
0.     10.     0.    10.     0.
3.      0.     0.     0.     3.
];
A = sparse(A);
spilu_ilutpplot(A);

// See on a 225-by-225 sparse matrix
path = spilu_getpath (  );
filename = fullfile(path,"tests","matrices","pde225.mtx");
A=mmread(filename);
[data,hLfil,hDrop,hPermtol,hBloc] = spilu_ilutpplot(A);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   </simplelist>
</refsection>
</refentry>

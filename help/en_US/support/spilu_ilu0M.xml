<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from spilu_ilu0M.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="spilu_ilu0M" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>spilu_ilu0M</refname><refpurpose>ILU(0) preconditioning (macro).</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [L,U] = spilu_ilu0M(A)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>A :</term>
      <listitem><para> n-by-n sparse real matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>L  :</term>
      <listitem><para> n-by-n sparse real matrix of doubles, lower triangular matrix</para></listitem></varlistentry>
   <varlistentry><term>U :</term>
      <listitem><para> n-by-n sparse real matrix of doubles, upper triangular matrix</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Builds an incomplete LU factorization of the sparse matrix <literal>A</literal>.
   </para>
   <para>
The nonzero pattern in L is the same as the nonzero pattern of the
lower part of A.
The nonzero pattern in U is the same as the nonzero pattern of the
upper part of A.
   </para>
   <para>
ILU0 is not recommended for realistic problems.
It is only provided for comparison purposes.
   </para>
   <para>
All the diagonal elements of the input matrix must be nonzero.
   </para>
   <para>
Uses ikj variant of Gaussian Elimination.
   </para>
   <para>
This function is a macro.
The purpose of this function is to reproduce the spilu_ilu0
function for small sparse matrices with a simple script.
This function is a Scilab port of Youcef Saad's Matlab ilu0
function.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// A small 3-by-3 matrix
// nnz(A)=7
A = [
1 2 0
3 4 5
0 6 7
];
B = sparse(A);
[L,U]=spilu_ilu0M(B);
full(L)
full(U)
Lexpected = [
1  0 0
3  1 0
0 -3 1
];
Uexpected = [
1  2 0
0 -2 5
0  0 22
];

// Preconditioning of a 237-by-237 matrix.
// nnz(A)=1017
path = spilu_getpath (  );
filename = fullfile(path,"tests","matrices","nos1.mtx");
A=mmread(filename);
[L,U]=spilu_ilu0M(A);
norm(A-L*U)

// To edit the code
edit spilu_ilu0M

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) Yousef Saad</member>
   </simplelist>
</refsection>
</refentry>

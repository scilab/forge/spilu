// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
//
// Generate the support functions help
helpdir = fullfile(cwd,"support");
mprintf("Generating help in %s...\n",helpdir);
funmat = [
  "spilu_getpath"
  "spilu_permVecToMat"
  "spilu_ilu0M"
  "spilu_ilukM"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the graphics functions help
helpdir = fullfile(cwd,"graphics");
mprintf("Generating help in %s...\n",helpdir);
funmat = [
  "spilu_iludplot"
  "spilu_iludpplot"
  "spilu_ilukplot"
  "spilu_ilutplot"
  "spilu_ilutpplot"
  "spilu_plotparameter"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
// Generate the benchmark functions help
helpdir = fullfile(cwd,"benchmark");
mprintf("Generating help in %s...\n",helpdir);
funmat = [
  "spilu_iluhub"
  "spilu_iluhubavail"
  "spilu_iluhubparname"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );


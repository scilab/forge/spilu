// ====================================================================
// Allan CORNET
// DIGITEO 2009
// This file is released into the public domain
// ====================================================================

function cleanMacros()
    libpath = get_absolute_file_path('cleanmacros.sce');

    binfiles = ls(libpath+'/*.bin');
    for i = 1:size(binfiles,'*')
        mdelete(binfiles(i));
    end

    mdelete(libpath+'/names');
    mdelete(libpath+'/lib');
endfunction

cleanMacros();
clear cleanMacros;

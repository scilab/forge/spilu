// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [data,hAlpha,hDrop,hPermtol,hBloc] = spilu_iludpplot(varargin)
    // Plots the sensitivity of ILUDP for A.
    // 
    // Calling Sequence
    //   spilu_iludpplot(A)
    //   data = spilu_iludpplot(A)
    //   data = spilu_iludpplot(A,N)
    //   [data,hAlpha] = spilu_iludplot(...)
    //   [data,hAlpha,hDrop] = spilu_iludplot(...)
    //   [data,hAlpha,hDrop,hPermtol] = spilu_iludplot(...)
    //   [data,hAlpha,hDrop,hPermtol,hBloc] = spilu_iludplot(...)
    //
    // Parameters
    // A: a n-by-n sparse matrix
    // N: a 1-by-1 matrix of doubles, integer value, the number of points in the plot (default N = 100)
    // data: a N-by-12 matrix of doubles, the values of the parameters
    // hAlpha: a graphics handle, the alpha plot
    // hDrop: a graphics handle, the drop plot
    // hPermtol: a graphics handle, the permtol plot
    // hBloc: a graphics handle, the bloc plot
    //
    // Description
    // For one particular matrix, plot norm(P*A-L*U)/norm(A) and nnz(L)+nnz(U), depending on 
    // alpha, drop, permtol and bloc for the ILUDP method.
    //
    // The columns of the array data are:
    // <itemizedlist>
    //     <listitem><para>
    //         data(:,1) : the values of alpha
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,2) : the values of norm(P*A-L*U)/norm(A) with respect to alpha
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,3) : the values of nnz(L)+nnz(U) with respect to alpha
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,4) : the values of drop
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,5) : the values of norm(P*A-L*U)/norm(A) with respect to drop
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,6) : the values of nnz(L)+nnz(U) with respect to drop
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,7) : the values of permtol
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,8) : the values of norm(P*A-L*U)/norm(A) with respect to permtol
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,9) : the values of nnz(L)+nnz(U) with respect to permtol
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,10) : the values of bloc
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,11) : the values of norm(P*A-L*U)/norm(A) with respect to bloc
    //     </para></listitem>
    //     <listitem><para>
    //         data(:,12) : the values of nnz(L)+nnz(U) with respect to bloc
    //     </para></listitem>
    // </itemizedlist>
    // 
    // If the decomposition does not work for one value of the parameter, 
    // then norm(A-L*U)/norm(A) and nnz(L)+nnz(U) are set to Infinity.
    // 
    // Examples
    // A = [
    //     30.    8.     7.     0.    0.  
    //     8.     18.    0.     0.   -1.  
    //     7.     0.     14.    0.    0.  
    //     0.     0.     0.     1.    0.  
    //     0.    -1.     0.     0.    2.  
    // ];
    // A = sparse(A);
    // spilu_iludpplot(A);
    // 
    // // See on a 225-by-225 sparse matrix
    // path = spilu_getpath (  );
    // filename = fullfile(path,"tests","matrices","pde225.mtx");
    // A=mmread(filename);
    // [data,hAlpha,hDrop,hPermtol,hBloc] = spilu_iludpplot(A);
    //
    // Authors
    //   Copyright (C) 2011 - DIGITEO - Michael Baudin

    function [pNorm,pNnz] = iludpCompute(L,U,perm)
        P = spilu_permVecToMat(perm);
        pNorm = norm(P*A-L*U,"inf")/norm(A,"inf");            
        pNnz = nnz(L) + nnz(U)
    endfunction

    function [pNorm,pNnz] = iludpDecompAlpha(A,alpha)
        instr = "[L,U,perm]=spilu_iludp(A,alpha)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            [pNorm,pNnz] = iludpCompute(L,U,perm)
        end
    endfunction

    function [pNorm,pNnz] = iludpDecompDrop(A,drop)
        instr = "[L,U,perm]=spilu_iludp(A,[],drop)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            [pNorm,pNnz] = iludpCompute(L,U,perm)
        end
    endfunction

    function [pNorm,pNnz] = iludpDecompPermtol(A,permtol)
        instr = "[L,U,perm]=spilu_iludp(A,[],[],permtol)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            [pNorm,pNnz] = iludpCompute(L,U,perm)
        end
    endfunction

    function [pNorm,pNnz] = iludpDecompBloc(A,bloc)
        instr = "[L,U,perm]=spilu_iludp(A,[],[],[],bloc)";
        ierr = execstr(instr,"errcatch");
        if (ierr<>0) then
            pNorm = %inf
            pNnz = %inf
        else
            [pNorm,pNnz] = iludpCompute(L,U,perm)
        end
    endfunction

    [lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_iludpplot" , rhs , 1:2 )
    apifun_checklhs ( "spilu_iludpplot" , lhs , 0:5 )
    // 
    // Get arguments
    A = varargin(1)
    N = apifun_argindefault ( varargin , 2 , 100 )
    //
    // Check type
    apifun_checktype ( "spilu_iludpplot" , A , "A" , 1 , "sparse" )
    apifun_checktype ( "spilu_iludpplot" , N , "N" , 2 , "constant" )
    //
    // Check size
    apifun_checksquare ( "spilu_iludpplot" , A , "A" , 1 )
    apifun_checkscalar ( "spilu_iludpplot" , N , "N" , 2 )
    //
    // Check content
    apifun_checkgreq ( "spilu_iludpplot" , N , "N" , 2 , 1 )
    //
    // Proceed...
    //
    // Compute decomposition for various values of alpha
    alpha = linspace(0,1,N)'
    hAlpha = scf();
    [pNormAlpha,pNnzAlpha] = spilu_plotparameter(A,"alpha",alpha,list(iludpDecompAlpha),%t)
    //
    // Compute decomposition for various values of drop
    drop = max(abs(A)) * logspace(-8,0,N)'
    hDrop = scf();
    [pNormDrop,pNnzDrop] = spilu_plotparameter(A,"drop",drop,list(iludpDecompDrop),%t)
    hDrop.children(1).log_flags = "lnn"
    hDrop.children(2).log_flags = "lnn"
    //
    // Compute decomposition for various values of permtol
    permtol = max(abs(A)) * logspace(-5,0,N)'
    hPermtol = scf();
    [pNormPermtol,pNnzPermtol] = spilu_plotparameter(A,"permtol",permtol,list(iludpDecompPermtol),%t)
    hPermtol.children(1).log_flags = "lnn"
    hPermtol.children(2).log_flags = "lnn"
    //
    // Compute decomposition for various values of bloc
    n = size(A,"r")
    bloc = floor(linspace(1,n,N)')
    hBloc = scf();
    [pNormBloc,pNnzBloc] = spilu_plotparameter(A,"bloc",bloc,list(iludpDecompBloc),%t)
    //
    // Gather the results
    data = [...
    alpha,pNormAlpha,pNnzAlpha,..
    drop,pNormDrop,pNnzDrop,...
    permtol,pNormPermtol,pNnzPermtol,...
    bloc,pNormBloc,pNnzBloc...
    ]
endfunction

// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function name = spilu_iluhubparname(m,k)
    // Returns the name of a ILU parameter, given the index.
    // 
    // Calling Sequence
    //   name = spilu_iluhubparname(m,k)
    //
    // Parameters
    // m : 1-by-1 matrix of strings, the ILU method. Available methods are m = "ilu0", "ilud", "iludp", "iluk", "ilut", "ilutp", "milu0"
    // k : 1-by-1 matrix of doubles, integer value, the parameter index
    // name : a 1-by-1 matrix of strings, the name of the parameter
    //
    // Description
    // Given a ILU method m and a parameter index k, returns the name 
    // of this parameter.
    //
    // Examples
    // name = spilu_iluhubparname("ilud",1) // alph
    // name = spilu_iluhubparname("ilud",2) // drop
	//
	// // A small 3-by-3 matrix
	// // nnz(A)=7
	// A = [
	// 1 2 0
	// 3 4 5
	// 0 6 7
	// ];
	// A = sparse(A);
	// for experiment = 1 : 4
	//     select experiment
	//     case 1 then
	//         m = "ilu0";
	//         args = list();
	//     case 2 then
	//         m = "milu0";
	//         args = list();
	//     case 3 then
	//         m = "ilut";
	//         tol  = 0.0001;
	//         lfil = 3;
	//         args = list(lfil,tol);
	//     case 4 then
	//         m = "ilud";
	//         tol = 0.075 ;
	//         alph= 0.0 ;
	//         args = list(alph,tol);
	//     else
	//         error("Unknown method")
	//     end
	//     mprintf("Preconditioner #%d: %s\n",experiment,m)
	//     for k = 1 : length(args)
	//         name = spilu_iluhubparname(m,k);
	//         mprintf("    %s = %s\n",name,string(args(k)))
	//     end
	//     [L,U,perm]=spilu_iluhub(A,m,args(:));
	//     mprintf("    nnz(A) = %d\n", nnz(A))
	//     mprintf("    nnz(LU) = %d\n", nnz(L)+nnz(U))
	// end
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "spilu_iluhubparname" , rhs , 2:2 )
    apifun_checklhs ( "spilu_iluhubparname" , lhs , 1:1 )
    //
    // Check types
    apifun_checktype ( "spilu_iluhubparname" , m ,  "m" , 1 , "string" )
    apifun_checktype ( "spilu_iluhubparname" , k ,  "k" , 2 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "spilu_iluhubparname" , m ,  "m" , 1 )
    apifun_checkscalar ( "spilu_iluhubparname" , k ,  "k" , 2 )
    //
    // Proceed...
    errfmt = "%s: Unknown parameter %d for method %s"
    select m
    case "ilu0"
        error(msprintf(errfmt,"spilu_iluhubparname",k,m))
    case "ilud" then
        select k
        case 1 then
            name = "alpha"
        case 2 then
            name = "drop"
        else
            error(msprintf(errfmt,"spilu_iluhubparname",k,m))
        end
    case "iludp" then
        select k
        case 1 then
            name = "alpha"
        case 2 then
            name = "drop"
        case 3 then
            name = "permtol"
        case 4 then
            name = "bloc"
        else
            error(msprintf(errfmt,"spilu_iluhubparname",k,m))
        end
    case "iluk" then
        select k
        case 1 then
            name = "lfil"
        else
            error(msprintf(errfmt,"spilu_iluhubparname",k,m))
        end
    case "ilut" then
        select k
        case 1 then
            name = "lfil"
        case 2 then
            name = "drop"
        else
            error(msprintf(errfmt,"spilu_iluhubparname",k,m))
        end
    case "ilutp" then
        select k
        case 1 then
            name = "lfil"
        case 2 then
            name = "drop"
        case 3 then
            name = "permtol"
        case 4 then
            name = "bloc"
        else
            error(msprintf(errfmt,"spilu_iluhubparname",k,m))
        end
    case "milu0" then
        error(msprintf(errfmt,"spilu_iluhubparname",k,m))
    else
        error(msprintf("%s: Unknown method %s","spilu_iluhub",m))
    end
endfunction

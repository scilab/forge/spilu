// ====================================================================
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================


function [pNorm,pNnz] = spilu_plotparameter(A,pname,pmatrix,__fundecomp__,perm)
    // Plots the sensitivity of a decomposition algorithm.
    // 
    // Calling Sequence
    //   spilu_plotparameter(A,pname,pmatrix,fundecomp,perm)
    //   pNorm = spilu_plotparameter(...)
    //   [pNorm,pNnz] = spilu_plotparameter(...)
    //
    // Parameters
    // A: a n-by-n sparse matrix
    // pname: a 1-by-1 matrix of strings, the name of the parameter
    // pmatrix: a N-by-1 matrix of doubles, the values of the parameter
    // fundecomp: a list, the decomposition algorithm
    // perm : : a N-by-1 matrix of booleans, set perm=%t if the algorithm has permutation, set perm=%f if not.
    // pNorm : a N-by-1 matrix of doubles, the value of norm(A-L*U,"inf")/norm(A,"inf") if perm==%f, the value of norm(P*A-L*U,"inf")/norm(A,"inf") if perm==%t
    // pNnz : a N-by-1 matrix of doubles, the value of nnz(L)+nnz(U)
    //
    // Description
    // For one particular matrix, plots the norm(A-L*U) and nnz(L)+nnz(U), depending on 
    // p for the given decomposition algorithm.
    //
    // The function fundecomp is a list (f,a1,a2,...,an), 
    // where the first element f is a function with header:
    //
	// <screen>
    // [pNorm,pNnz] = f(A,p,a1,a2,...,an)
	// </screen>
    //
    // where A is the sparse matrix, p is the current value of the parameter, 
    // pNorm is the current value of the norm, 
    // pNnz is the current value of nnz(L)+nnz(U) and 
    // a1, a2, ..., an are extra-arguments which are automatically added 
    // to the calling sequence of f.
    // The value of pNorm should be norm(A-L*U,"inf")/norm(A,"inf") 
    // if perm==%f, norm(P*A-L*U,"inf")/norm(A,"inf") if perm==%t.
    // 
    // Examples
    // function [pNorm,pNnz] = iludDecompAlpha(A,alpha)
    //     instr = "[L,U]=spilu_ilud(A,alpha)";
    //     ierr = execstr(instr,"errcatch");
    //     if (ierr<>0) then
    //         pNorm = %inf
    //         pNnz = nnz(A)
    //     else
    //         pNorm = norm(A-L*U,"inf");            
    //         pNnz = nnz(L) + nnz(U)
    //     end
    // endfunction
    // A = [
    //     34.    4.   -10.    0.  -3.  
    //     4.     24.  -8.     0.    0.  
    //   -10.  -8.     36.    0.    0.  
    //     0.     0.     0.     1.    0.  
    //   -3.     0.     0.     0.    6.  
    // ];
	// A = sparse(A);
	// N = 100;
    // alpha = linspace(0,1,N)';
    // scf();
    // [pNorm,pNnz] = spilu_plotparameter(A,"alpha",alpha,..
	//     list(iludDecompAlpha),%f);
    //
    //
    // Authors
    //   Copyright (C) 2011 - DIGITEO - Michael Baudin

	[lhs,rhs]=argn()
    apifun_checkrhs ( "spilu_plotparameter" , rhs , 5 )
    apifun_checklhs ( "spilu_plotparameter" , lhs , 0:2 )
    //
    // Check type
    apifun_checktype ( "spilu_plotparameter" , A , "A" , 1 , "sparse" )
    apifun_checktype ( "spilu_plotparameter" , pname , "pname" , 2 , "string" )
    apifun_checktype ( "spilu_plotparameter" , pmatrix , "pmatrix" , 3 , "constant" )
    apifun_checktype ( "spilu_plotparameter" , __fundecomp__ , "fundecomp" , 4 , "list" )
    //
    // Check size
    apifun_checksquare ( "spilu_plotparameter" , A , "A" , 1 )
    apifun_checkscalar ( "spilu_plotparameter" , pname , "pname" , 2 )
    apifun_checkveccol ( "spilu_plotparameter" , pmatrix , "pmatrix" , 3 )
	//
	// Proceed...

    __fundecomp_f__ = __fundecomp__(1)
    //
    // Compute norm(A-L*U) for various values of p
    N = size(pmatrix,"*")
    for i = 1 : N
        p = pmatrix(i)
        [pNormI,pNnzI] = __fundecomp_f__(A,p,__fundecomp__(2:$))
        pNorm(i) = pNormI
        pNnz(i) = pNnzI
    end
    //
    // Plot this
    // Plot norm vs p
    subplot(2,1,1)
    plot(pmatrix,pNorm)
    if ( perm ) then
        ylabstr = "norm(P*A-L*U)/norm(A)"
    else
        ylabstr = "norm(A-L*U)/norm(A)"
    end
    xtitle("Sensitivity of decomposition vs "+pname,pname,ylabstr)
    // Plot nnz vs alpha
    subplot(2,1,2)
    plot(pmatrix,pNnz)
    xtitle("Number of nonzeros in decomposition vs "+pname,pname,"nnz(L) + nnz(U)")
    //
    // Change the bounds of the nnz plot, 
    // to make the line visible
    h = gcf()
    h = h.children(1)
    h.data_bounds(1,2) = h.data_bounds(1,2) - 1
    h.data_bounds(2,2) = h.data_bounds(2,2) + 1
endfunction

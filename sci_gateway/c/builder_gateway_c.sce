// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function buildGwC()

    gateway_path = get_absolute_file_path("builder_gateway_c.sce");

    libname = "spilugateway";
    namelist = [
    "spilu_ilut"  "sci_ilut"
    "spilu_ilud"  "sci_ilud"
    "spilu_ilutp" "sci_ilutp"
    "spilu_iludp" "sci_iludp"
    "spilu_iluk"  "sci_iluk"
    "spilu_ilu0"  "sci_ilu0"
    "spilu_milu0" "sci_milu0"
    ];
    files = [
    "sci_ilut.c"
    "sci_ilud.c"
    "sci_ilutp.c"
    "sci_iludp.c"
    "sci_iluk.c"
    "sci_ilu0.c"
    "sci_milu0.c"
    "gw_spilu_support.c"
    ];


    ldflags = ""

    if getos() == "Windows" then
        include2 = "..\..\src\fortran";
        include3 = "..\..\src\c";
        include4 = SCI+"/modules/output_stream/includes";
        cflags = "-DWIN32 -I"""+include2+""" -I"""+include3+""" -I"""+include4+"""";
        libs = [
        "..\..\src\fortran\libspiluf"
        "..\..\src\c\libspiluc"
        ];
    else
        include1 = gateway_path;
        include2 = gateway_path+"../../src/fortran";
        include3 = gateway_path+"../../src/c";
        include4 = SCI+"/../../include/scilab/localization";
        include5 = SCI+"/../../include/scilab/output_stream";
        include6 = SCI+"/../../include/scilab/core";
        cflags = "-I"""+include1+""" -I"""+include2+""" -I"""+include3+...
        """ -I"""+include4+""" -I"""+include5+""" -I"""+include6+"""";
        libs = [
        "../../src/fortran/libspiluf"
        "../../src/c/libspiluc"
        ];
    end

    tbx_build_gateway(libname, namelist, files, gateway_path, libs, ldflags, cflags);

endfunction
buildGwC();
clear buildGwC;


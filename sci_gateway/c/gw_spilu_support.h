
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //

//
// gw_splpsc_support.h
//   Support functions for the SPILU gateway functions.
//
#ifndef __SCI_GW_SPILU_SUPPORT_H__
#define __SCI_GW_SPILU_SUPPORT_H__

#include "scisparse.h"

/* Functions generic of any interface */
int spilu_getScalarDouble(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        double defaultdouble, double * mydouble);
int spilu_checkDoubleInRange(
        char * fname, void * pvApiCtx,
        int ivar,
        double mydouble, double mindouble, double maxdouble);
int spilu_getScalarIntegerFromScalarDouble(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        int defaultint, int * myint);
int spilu_checkIntegerInRange(
        char * fname, void * pvApiCtx,
        int ivar,
        int myint, int minint, int maxint);
int spilu_getSparseMatrix(
        char * fname, void * pvApiCtx,
        int ivar, SciSparse * A);

/* Functions specific to this module. */
int spilu_getArgumentDrop(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        SciSparse A, double * drop);
int spilu_getArgumentAlpha(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        double * alpha);
int spilu_getArgumentPermtol(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        double * permtol);
int spilu_getArgumentBloc(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        SciSparse A, int * bloc);
int spilu_getArgumentLfil(
        char * fname, void * pvApiCtx,
        int ivar, int rhs,
        SciSparse A, int * lfil);

/* Allocate the memory */
int spilu_allocLUArrays(
        char * fname, void * pvApiCtx,
        int nnzLU, int nrows,
        double ** alu, int ** jlu, int ** ju);
int spilu_allocRowptr(
        char * fname, void * pvApiCtx,
        SciSparse A, int ** ia);

#endif /* __SCI_GW_SPILU_SUPPORT_H__ */

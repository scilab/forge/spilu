// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// From Scilab
#include "api_scilab.h"
#include "Scierror.h"
#include "scisparse.h"

// From ANSI C
#include "stdlib.h"
#include "float.h"

// From this gateway
#include "gw_spilu_support.h"

// From this lib
#include "conv.h"
#include "spilu.h"

extern int sci_ilud(char * fname, void * pvApiCtx)
{
    int palp = 0;
    int pdt = 0;
    int iwk = 0;
    int * jw = NULL;
    int * jlu = NULL;
    int * ju = NULL;
    int * ia = NULL;
    double * w = NULL;
    double * alu = NULL;
    SciSparse A;
    SciSparse * L = NULL;
    SciSparse * U = NULL;
    double alpha;
    double drop;
    double defaultdrop;
    int nnzLU = 0;

    SciErr sciErr;
    int iErr;
    int readFlag;

    CheckInputArgument(pvApiCtx, 1, 3);
    CheckOutputArgument(pvApiCtx, 2, 2);

    // Argument 1: A (sparse double)
    readFlag = spilu_getSparseMatrix(fname, pvApiCtx, 1, &A);
    if ( readFlag == FALSE )
    {
        goto sci_ilud_free_return;
    }
    if (A.m != A.n)
    {
        Scierror(501, "%s: input matrix must be square.\n", fname);
        goto sci_ilud_free_return;
    }

    // Argument 2: alpha (double), positive
    readFlag = spilu_getArgumentAlpha(fname, pvApiCtx, 2, Rhs, &alpha);
    if ( readFlag == FALSE )
    {
        goto sci_ilud_free_return;
    }

    // Argument 3: drop (double), positive
    readFlag = spilu_getArgumentDrop(fname, pvApiCtx, 3, Rhs, A, &drop);
    if ( readFlag == FALSE)
    {
        goto sci_ilud_free_return;
    }

    // Proceed...
    readFlag = spilu_allocRowptr(fname, pvApiCtx, A, &ia);
    if ( readFlag == FALSE )
    {
        goto sci_ilud_free_return;
    }
    nnzLU = spiluc_lband(A) + A.m + 1;
    readFlag = spilu_allocLUArrays(
            fname, pvApiCtx, nnzLU, A.m, &alu, &jlu, &ju);
    if ( readFlag == FALSE )
    {
        goto sci_ilud_free_return;
    }

    jw = (int *)malloc(2 * A.m * sizeof(int));
    if (jw == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        goto sci_ilud_free_return;
    }
    w = (double *)malloc(A.m * sizeof(double));
    if (w == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        goto sci_ilud_free_return;
    }

    iwk = nnzLU + 1;

    F2C(ilud)(&A.m, A.R, A.icol, ia, &alpha, &drop,
              alu, jlu, ju, &iwk, w, jw, &iErr);
    free(w);  w  = NULL;
    free(jw); jw = NULL;
    free(ia); ia = NULL;

    // Error case
    if (iErr)
    {
        switch (iErr)
        {
        case -1 :
            Scierror(501,"%s: input matrix may be wrong.\n", fname);
            goto sci_ilud_free_return;
        case -2 :
            Scierror(501,"%s: not enough memory for matrix L or U.\n", fname);
            goto sci_ilud_free_return;
        case -3 :
            Scierror(501,"%s: zero row encountered.\n", fname);
            goto sci_ilud_free_return;
        default :
            Scierror(501,"%s: zero pivot encountered at step number %d.\n",
                     fname, iErr);
            goto sci_ilud_free_return;
        }
    }

    // Regular case
    spiluc_spluget(A.m, ju, jlu, alu, &L, &U);
    free(ju);  ju  = NULL;
    free(jlu); jlu = NULL;
    free(alu); alu = NULL;

    if ( L->it == 0 )
    {
        createSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 1,
                L->m, L->n, L->nel, L->mnel, L->icol, L->R);
    }
    else
    {
        createComplexSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 1,
                L->m, L->n, L->nel, L->mnel, L->icol, L->R, L->I);
    }
    if ( U->it == 0 )
    {
        createSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 2,
                U->m, U->n, U->nel, U->mnel, U->icol, U->R);
    }
    else
    {
        createComplexSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 2,
                U->m, U->n, U->nel, U->mnel, U->icol, U->R, U->I);
    }

    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
    AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;

    return 0;

sci_ilud_free_return :

    if (w)      free(w);
    if (jw)     free(jw);
    if (ia)     free(ia);
    if (ju)     free(ju);
    if (jlu)    free(jlu);
    if (alu)    free(alu);
    if (L)
    {
        if (L->mnel) free(L->mnel);
        if (L->icol) free(L->icol);
        if (L->R)    free(L->R);
        if (L->I)    free(L->I);
    }
    if (U)
    {
        if (U->mnel) free(U->mnel);
        if (U->icol) free(U->icol);
        if (U->R)    free(U->R);
        if (U->I)    free(U->I);
    }

    return 1;

}

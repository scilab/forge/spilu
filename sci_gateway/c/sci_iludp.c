// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// From Scilab
#include "api_scilab.h"
#include "Scierror.h"
#include "scisparse.h"

// From the lib
#include "conv.h"
#include "spilu.h"

// From ANSI C
#include "stdlib.h"
#include "stdio.h"

// From this gateway
#include "gw_spilu_support.h"


extern int sci_iludp(char * fname, void * pvApiCtx)
{
    int i = 0;
    int mperm = 1;
    int nperm = 0;
    int iwk = 0;
    int * jw = NULL;
    int * jlu = NULL;
    int * ju = NULL;
    int * vperm = NULL;
    double * pperm = NULL;
    int * ia = NULL;
    double * w = NULL;
    double * alu = NULL;
    SciSparse A;
    SciSparse * L = NULL;
    SciSparse * U = NULL;
    double alpha;
    double drop;
    double permtol;
    int bloc;
    int nnzLU;

    SciErr sciErr;
    int iErr;
    int readFlag;

    CheckInputArgument(pvApiCtx, 1, 5);
    CheckOutputArgument(pvApiCtx, 3, 3);

    // Argument 1: A (sparse double)
    readFlag = spilu_getSparseMatrix(fname, pvApiCtx, 1, &A);
    if ( readFlag == FALSE )
    {
        goto sci_iludp_free_return;
    }
    if ( A.m != A.n )
    {
        Scierror(501, "%s: input matrix must be square.\n", fname);
        goto sci_iludp_free_return;
    }

    // Argument 2: alpha (double) positive
    readFlag = spilu_getArgumentAlpha(fname, pvApiCtx, 2, Rhs, &alpha);
    if ( readFlag == FALSE )
    {
        goto sci_iludp_free_return;
    }

    // Argument 3: drop (double) positive
    readFlag = spilu_getArgumentDrop(fname, pvApiCtx, 3, Rhs, A, &drop);
    if ( readFlag == FALSE )
    {
        goto sci_iludp_free_return;
    }

    // Argument 4: permtol (double) positive
    readFlag = spilu_getArgumentPermtol(fname, pvApiCtx, 4, Rhs, &permtol);
    if ( readFlag == FALSE )
    {
        goto sci_iludp_free_return;
    }

    // Argument 5: bloc (double) positive, integer value
    readFlag = spilu_getArgumentBloc(fname, pvApiCtx, 5, Rhs, A, &bloc);
    if ( readFlag == FALSE )
    {
        goto sci_iludp_free_return;
    }

    // Proceed...
    nperm = A.m;
    sciErr = allocMatrixOfDouble(
            pvApiCtx, nbInputArgument(pvApiCtx) + 1,
            mperm, nperm, &pperm);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        goto sci_iludp_free_return;
    }

    readFlag = spilu_allocRowptr(fname, pvApiCtx, A, &ia);
    if ( readFlag == FALSE )
    {
        goto sci_iludp_free_return;
    }
    nnzLU = spiluc_lband(A) + A.m + 1;
    readFlag = spilu_allocLUArrays(
            fname, pvApiCtx, nnzLU, A.m, &alu, &jlu, &ju);
    if ( readFlag == FALSE )
    {
        goto sci_iludp_free_return;
    }

    iwk = nnzLU + 1;

    jw = (int *)malloc(2 * A.m * sizeof(int));
    if (jw == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        goto sci_iludp_free_return;
    }
    w = (double *)malloc(2 * A.m * sizeof(double));
    if (w == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        goto sci_iludp_free_return;
    }
    vperm = (int *)malloc(2 * A.m * sizeof(int));
    if (vperm == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        goto sci_iludp_free_return;
    }

    F2C(iludp)(&A.m, A.R, A.icol, ia, &alpha, &drop,
               &permtol, &bloc, alu, jlu, ju, &iwk,
               w, jw, vperm, &iErr);

    free(w);  w  = NULL;
    free(jw); jw = NULL;
    free(ia); ia = NULL;

    // Error case
    if (iErr)
    {
        switch (iErr)
        {
        case -1 :
            Scierror(501,"%s: input matrix may be wrong.\n", fname);
            goto sci_iludp_free_return;
        case -2 :
            Scierror(501,"%s: not enough memory for matrix L or U.\n", fname);
            goto sci_iludp_free_return;
        case -3 :
            Scierror(501,"%s: zero row encountered.\n", fname);
            goto sci_iludp_free_return;
        default :
            Scierror(501,"%s: zero pivot encountered at step number %d.\n",
                     fname, iErr);
            goto sci_iludp_free_return;
        }
    }

    // Regular case
    for (i=0; i<nperm; i++)
    {
        pperm[i] = vperm[i];
    }
    free(vperm); vperm = NULL;

    spiluc_spluget(A.m, ju, jlu, alu, &L, &U);
    free(ju);  ju  = NULL;
    free(jlu); jlu = NULL;
    free(alu); alu = NULL;

    if ( L->it == 0 )
    {
        createSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 2,
                L->m, L->n, L->nel, L->mnel, L->icol, L->R);
    }
    else
    {
        createComplexSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 2,
                L->m, L->n, L->nel, L->mnel, L->icol, L->R, L->I);
    }
    if ( U->it == 0 )
    {
        createSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 3,
                U->m, U->n, U->nel, U->mnel, U->icol, U->R);
    }
    else
    {
        createComplexSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 3,
                U->m, U->n, U->nel, U->mnel, U->icol, U->R, U->I);
    }

    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 2;
    AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 3;
    AssignOutputVariable(pvApiCtx, 3) = nbInputArgument(pvApiCtx) + 1;

    return 0;

sci_iludp_free_return :

    if (w)     free(w);
    if (jw)    free(jw);
    if (ia)    free(ia);
    if (ju)    free(ju);
    if (jlu)   free(jlu);
    if (alu)   free(alu);
    if (vperm) free(vperm);
    if (L)
    {
        if (L->mnel) free(L->mnel);
        if (L->icol) free(L->icol);
        if (L->R)    free(L->R);
        if (L->I)    free(L->I);
    }
    if (U)
    {
        if (U->mnel) free(U->mnel);
        if (U->icol) free(U->icol);
        if (U->R)    free(U->R);
        if (U->I)    free(U->I);
    }

    return 1;

}

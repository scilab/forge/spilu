// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// From Scilab
#include "api_scilab.h"
#include "Scierror.h"
#include "scisparse.h"

// From the lib
#include "conv.h"
#include "spilu.h"

// From ANSI C
#include "stdlib.h"

// From this gateway
#include "gw_spilu_support.h"

extern int sci_milu0(char * fname, void * pvApiCtx)
{
    int nnzLU = 0;
    int * ia = NULL;
    int * iw = NULL;
    int * jlu = NULL;
    int * ju = NULL;
    double * alu = NULL;
    SciSparse A;
    SciSparse * L = NULL;
    SciSparse * U = NULL;

    SciErr sciErr;
    int iErr;
    int readFlag;

    CheckInputArgument(pvApiCtx, 1, 1);
    CheckOutputArgument(pvApiCtx, 2, 2);

    // Argument 1: A (sparse)
    readFlag = spilu_getSparseMatrix(fname, pvApiCtx, 1, &A);
    if ( readFlag == FALSE)
    {
        goto sci_milu0_free_return;
    }
    if ( A.m != A.n )
    {
        Scierror(501, "%s: input matrix must be square.\n", fname);
        goto sci_milu0_free_return;
    }

    // Proceed...
    readFlag = spilu_allocRowptr(fname, pvApiCtx, A, &ia);
    if ( readFlag == FALSE )
    {
        goto sci_milu0_free_return;
    }
    nnzLU = A.nel + A.m;
    readFlag = spilu_allocLUArrays(
            fname, pvApiCtx, nnzLU, A.m, &alu, &jlu, &ju);
    if ( readFlag == FALSE )
    {
        goto sci_milu0_free_return;
    }
    iw = (int *)malloc(A.m * sizeof(int));
    if (iw == NULL)
    {
        Scierror(112, "%s: No more memory.\n", fname);
        goto sci_milu0_free_return;
    }

    F2C(milu0)(&A.m, A.R, A.icol, ia, alu, jlu, ju, iw, &iErr);

    free(iw); iw = NULL;
    free(ia); ia = NULL;

    // Error case
    if (iErr)
    {
        Scierror(501, "%s: zero pivot encountered at step number %d.\n",
                 fname, iErr);
        goto sci_milu0_free_return;
    }

    // Regular case
    spiluc_spluget(A.m, ju, jlu, alu, &L, &U);
    free(ju);  ju  = NULL;
    free(jlu); jlu = NULL;
    free(alu); alu = NULL;

    if ( L->it == 0 )
    {
        createSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 1,
                L->m, L->n, L->nel, L->mnel, L->icol, L->R);
    }
    else
    {
        createComplexSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 1,
                L->m, L->n, L->nel, L->mnel, L->icol, L->R, L->I);
    }
    if ( U->it == 0 )
    {
        createSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 2,
                U->m, U->n, U->nel, U->mnel, U->icol, U->R);
    }
    else
    {
        createComplexSparseMatrix(
                pvApiCtx, nbInputArgument(pvApiCtx) + 2,
                U->m, U->n, U->nel, U->mnel, U->icol, U->R, U->I);
    }

    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
    AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;

    return 0;

sci_milu0_free_return :

    if (iw)     free(iw);
    if (ia)     free(ia);
    if (ju)     free(ju);
    if (jlu)    free(jlu);
    if (alu)    free(alu);
    if (L)
    {
        if (L->mnel) free(L->mnel);
        if (L->icol) free(L->icol);
        if (L->R)    free(L->R);
        if (L->I)    free(L->I);
    }
    if (U)
    {
        if (U->mnel) free(U->mnel);
        if (U->icol) free(U->icol);
        if (U->R)    free(U->R);
        if (U->I)    free(U->I);
    }

    return 1;

}

//
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This toolbox is released under the terms of the CeCILL license :
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//


#include <math.h>
#include <stdlib.h>

#include "conv.h"

// Compute the index of the begining of each row in A.
//
// Arguments
// SciSparse (input) : a Scilab sparse matrix.
// ia[0:nrows] (output) : an array with A.m+1 entries, pointers to the rows of A.
//            For i=1, 2,..., A.m, the value ia(i) is the first
//            index of a for the row A(i,:).
//            In other words, for i=1, 2,..., A.m,
//            a(ia(i):ia(i+1)-1) contains the nonzeros of the i-th row.
//            The number of nonzeros is : ia(A.m+1)-1
//            This is the "row_ptr" array in the CSR format.
//
// Description
// Compute cumulated values of the mnel array.
// In the Compressed Sparse Row (CSR) format, this is
// row_ptr.
//
// A.m (input) : the number of rows in A
// A.mnel[0:nrows-1] (input) : an array with A.m entries.
//                 For i= 1, 2, ..., A.m, the entry mnel[i-1] is
//                 the number of nonzeros of the i-th row.
//
void spiluc_Sci2spk(SciSparse A, int * ia)
{
    int i;

    ia[0] = 1;
    for (i=1; i < A.m+1 ; i++)
    {
        ia[i] = ia[i-1] + A.mnel[i-1];
    }

    return ia;
}



// Returns the sum of the bandwidths of all rows.
//
// Arguments
// A (input): a Scilab sparse matrix
// bd (output): integer, the sum of the bandwidths of all rows
//
// Description
// For each row, the bandwidth is icolMax - icolMin + 1,
// where icolMax is maximum column index of a nonzero entry in the
// row and icolMin is minimum column index of a nonzero entry in the
// row.
// This function returns the sum of each row bandwith.
//
// Example:
// A 6-by-6 sparse matrix
// nnz(A) = 16
// A=[
// -1.    3.    0.    0.    4.    0.
// 2.   -5.    0.    0.    0.    1.
// 0.    0.   -2.    3.    0.    0.
// 0.    0.    7.   -1.    0.    0.
// -3.    0.    0.    4.    6.    0.
// 0.    5.    0.    0.   -7.    8.
// ];
//
// Here, the value of bd is : 25 = 5+6+2+2+5+5.
// Indeed, for the row #1, the bandwidth is 5-1+1=5 and
// for the row #3, the bandwidth is 4-3+1=2.
//
int spiluc_lband(SciSparse A)
{
    int i = 0;
    int bd = 0;
    int brow = 0;
    int erow = 0;

    for (i=0; i<A.m; i++)
    {
        if (A.mnel[i])
        {
            erow = brow + A.mnel[i] - 1;
            bd += A.icol[erow] - A.icol[brow] + 1;
            brow = erow + 1;
        }
    }
    return bd;
}

// Returns the maximum of the absolute value of
// the entries in the matrix.
//
// Arguments
// eltm: a double, the maximum
//
// Description
// This function is equivalent to :
//
//  eltm = max(abs(A))
//
// in the Scilab syntax.
//
double spiluc_eltm(SciSparse A)
{
    int i = 0;
    double m = 0;

    for (i=0; i<A.nel; i++)
    {
        if (fabs(A.R[i]) > m)
        {
            m = fabs(A.R[i]);
        }
    }
    return m;
}

// Convert the MSR of the LU factors of A into CSR
//
// Arguments
// Input:
// n: a 1-by-1 matrix of doubles, the size of the matrix
// ju: a n-by-1 matrix of doubles, pointer to the diagonal elements in alu, jlu
// alu: a (nnz+1)-by-1 matrix of doubles, the values of the ALU factors
// jlu: a (nnz+1)-by-1 matrix of doubles, pointers to the alu array
//
// Output:
// L: a Scilab sparse matrix, the sparse representation of the L factors
// U: a Scilab sparse matrix, the sparse representation of the U factors
//
// Description
// Convert the  Modified Sparse Row (MSR) format
// into Compressed Sparse Row (CSR) format.
// We make the assumption that A is square,
// i.e. is a n-by-n sparse matrix.
//
// The diagonal of the L is unity, and this is why it is
// not stored.
// The L and U factors are stored in one single matrix:
//
// LU =
//     U(1,1) U(1,2) U(1,3) ... U(1,n)
//     L(2,1) U(2,2) U(2,3) ... U(2,n)
//     ...
//     L(n,1) L(n,2)    ... ... U(n,n)
//
// This LU matrix is sparse and is stored in MSR format.
//
// The Modified Sparse Row (MSR) format is a
// variation of the Compressed Sparse Row format which consists
// of keeping the main diagonal of the LU factors separately.
// The corresponding data structure consists of a real array alu
// and two integer arrays jlu and ju.
//
// The alu array is made of two parts:
//  * alu(1:n) is the inverted diagonal of the U factors.
//  * alu(n+1) is not used.
//  * alu(n+2:$) : the nonzero elements of the LU factors,
//                 excluding its diagonal elements, are stored row-wise.
//
// The jlu array is made of two parts:
//  * jlu(1:n) : jlu(k) is the begininning of the k-th row in alu,
//               i.e. alu(jlu(k)) is the first nonzero of the k-th row of LU.
//  * jlu(n+1) is not used.
//  * jlu(n+2:$) : jlu(k) is the column index of the element alu(k).
//
// The ju array separates the L and U parts in each row:
//  * ju(1:n) : ju(k) is the indice in alu where the U factors starts,
//              i.e. alu(ju(k)) is the first nonzero of the k-th row of U.
//
// Bibliography
//  "SPARSKIT: A basic tool-kit for sparse matrix computations",
//  Youcef Saad, 1994
//  See section 2.1.1 Compressed Sparse Row and related formats
int spiluc_spluget(int n, int *ju, int * jlu, double * alu,
                   SciSparse ** L, SciSparse ** U)
{
    int i=0,row=0,indLU=n+1,indL=0,indU=0;

    *L=(SciSparse *) malloc(sizeof(SciSparse));
    (*L)->m=n;
    (*L)->n=n;
    (*L)->it=0;
    (*L)->nel=0;
    (*L)->mnel=(int *) malloc(n*sizeof(int));

    *U=(SciSparse *) malloc(sizeof(SciSparse));
    (*U)->m=n;
    (*U)->n=n;
    (*U)->it=0;
    (*U)->nel=0;
    (*U)->mnel=(int *) malloc(n*sizeof(int));

    for (i=0; i<n ;i++){
        (*L)->mnel[i]=ju[i]-jlu[i]+1;
        (*L)->nel+=(*L)->mnel[i];
        (*U)->mnel[i]=jlu[i+1]-ju[i]+1;
        (*U)->nel+=(*U)->mnel[i];
    }

    (*L)->icol=(int *) malloc((*L)->nel*sizeof(int));
    (*L)->R=(double *) malloc((*L)->nel*sizeof(double));

    (*U)->icol=(int *) malloc((*U)->nel*sizeof(int));
    (*U)->R=(double *) malloc((*U)->nel*sizeof(double));

    for (row=0;row<n;row++){
        for(i=0;i<(*L)->mnel[row]-1 ;i++){
            (*L)->icol[indL]=jlu[indLU];
            (*L)->R[indL]=alu[indLU];
            indL++;
            indLU++;
        }

        (*L)->icol[indL]=row+1;
        (*L)->R[indL]=1;
        indL++;

        (*U)->icol[indU]=row+1;
        (*U)->R[indU]=1/(alu[row]);
        indU++;

        for(i=0;i<(*U)->mnel[row]-1;i++){
            (*U)->icol[indU]=jlu[indLU];
            (*U)->R[indU]=alu[indLU];
            indU++;
            indLU++;
        }
    }

    return(0);
}


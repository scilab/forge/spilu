// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function buildFortran()

    fortran_dir = get_absolute_file_path("builder_fortran.sce");

    fortran_path = "f";
    linknames = ["spiluf"];
    files = [
    "ilu0.f"
    "ilut.f"
    "ilutp.f"
    "ilud.f"
    "iludp.f"
    "iluk.f"
    "milu0.f"
    "qsplit.f"
    ];
    ldflags = "";

    fflags = "";

    if getos() == "Windows" then
        cflags = "-DWIN32 -DLIBSPILUF_EXPORTS";
        libs = [];
    else
        include1 = fortran_dir;
        cflags = "-I"""+include1+"""";
        libs = [];
    end

    tbx_build_src(linknames, files, fortran_path, fortran_dir, libs, ldflags, cflags, fflags);

endfunction

buildFortran();
clear buildFortran;


C Copyright (C) 1993 - Univ. of Tennessee and Oak Ridge National Laboratory
C Copyright (C) 2000 - 2001 - INRIA - Aladin Group
C Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
C Copyright (C) 2011 - NII - Benoit Goepfert

        subroutine ilu0(n, a, ja, ia, alu, jlu, ju, iw, ierr)
        implicit none
        integer ju0, n, i, ii, js, j, jcol, jf, jm
        integer jrow, jj, jw, ierr
        double precision tl
        double precision a(*), alu(*)
        integer ja(*), ia(*), ju(*), jlu(*), iw(*)
        integer nnz
c------------------ right preconditioner ------------------------------*
c                    ***   ilu(0) preconditioner.   ***                *
c----------------------------------------------------------------------*
c Note that this has been coded in such a way that it can be used
c with pgmres. Normally, since the data structure of the L+U matrix is
c the same as that the A matrix, savings can be made. In fact with
c some definitions (not correct for general sparse matrices) all we
c need in addition to a, ja, ia is an additional diagonal.
c ILU0 is not recommended for serious problems. It is only provided
c here for comparison purposes.
c-----------------------------------------------------------------------
c
c on entry:
c---------
c n       = dimension of matrix
c a, ja,
c ia      = original matrix in compressed sparse row storage.
c
c on return:
c-----------
c alu,jlu = matrix stored in Modified Sparse Row (MSR) format containing
c           the L and U factors together. The diagonal (stored in
c           alu(1:n) ) is inverted. Each i-th row of the alu,jlu matrix
c           contains the i-th row of L (excluding the diagonal entry=1)
c           followed by the i-th row of U.
c
c ju	  = pointer to the diagonal elements in alu, jlu.
c
c ierr	  = integer indicating error code on return
c	     ierr = 0 --> normal return
c	     ierr = k --> code encountered a zero pivot at step k.
c work arrays:
c-------------
c iw	    = integer work array of length n.
c------------
c IMPORTANT
c-----------
c it is assumed that the the elements in the input matrix are stored
c    in such a way that in each row the lower part comes first and
c    then the upper part. To get the correct ILU factorization, it is
c    also necessary to have the elements of L sorted by increasing
c    column number. It may therefore be necessary to sort the
c    elements of a, ja, ia prior to calling ilu0. This can be
c    achieved by transposing the matrix twice using csrcsc.
C
C Modified by M. Baudin (2011): 
C * declare all variables explicitely
C * initialized ju to zero
C * initialized alu to zero
C
C The size and content of a, ia, ja, ju, alu and jlu.
C---------------------
C
C a(1:nnz): the nonzeros of A.
C           For i=1, 2,..., nnz, the value a(i) is the i-th nonzero of A.
C           This is the "val" array in the CSR format.
C ia(1:n+1): pointer to the rows of A.
C            For i=1, 2,..., n, the value ia(i) is the first 
C            index of a for the row A(i,:).
C            In other words, for i=1, 2,..., n, 
C            a(ia(i):ia(i+1)-1) contains the nonzeros of the i-th row.
C            The number of nonzeros is : ia(n+1)-1
C            This is the "row_ptr" array in the CSR format.
C ja(1:nnz): the column indices of A.
C            For i=1,2,...nnz, ja(i) is the column index of the 
C            i-th nonzero of A.
C            This is the "col_ind" array in the CSR format.
C
C ju(1:n): pointer to the diagonal elements in alu, jlu
C alu(1:nnz+1): the values of the ALU factors
C jlu(1:nnz+1): pointers to the alu array
C
c-----------------------------------------------------------------------

      nnz = ia(n+1)-1
C
C Initialize ju
C
      do i=1, n
           ju(i) = 0
      enddo
C
C Initialize alu
C
      do i=1, nnz
           alu(i) = 0
      enddo

        ju0 = n+2
        jlu(1) = ju0
c
c initialize work vector to zero's
c
	do 31 i=1, n
           iw(i) = 0
 31     continue
c
c main loop
c
	do 500 ii = 1, n
           js = ju0
c
c generating row number ii of L and U.
c
           do 100 j=ia(ii),ia(ii+1)-1
c
c     copy row ii of a, ja, ia into row ii of alu, jlu (L/U) matrix.
c
              jcol = ja(j)
              if (jcol .eq. ii) then
                 alu(ii) = a(j)
                 iw(jcol) = ii
                 ju(ii)  = ju0
              else
                 alu(ju0) = a(j)
                 jlu(ju0) = ja(j)
                 iw(jcol) = ju0
                 ju0 = ju0+1
              endif
 100       continue
           jlu(ii+1) = ju0
           jf = ju0-1
           jm = ju(ii)-1
c
c     exit if diagonal element is reached.
c
           do 150 j=js, jm
              jrow = jlu(j)
              tl = alu(j)*alu(jrow)
              alu(j) = tl
c
c     perform  linear combination
c
              do 140 jj = ju(jrow), jlu(jrow+1)-1
                 jw = iw(jlu(jj))
                 if (jw .ne. 0) then
                     alu(jw) = alu(jw) - tl*alu(jj)
                 endif
 140          continue
 150       continue
c
c     invert  and store diagonal element.
c
           if (alu(ii) .eq. 0.0d0) then
             goto 600
           endif
           alu(ii) = 1.0d0/alu(ii)
c
c     reset pointer iw to zero
c
           iw(ii) = 0
           do 201 i = js, jf
 201          iw(jlu(i)) = 0
 500       continue

           ierr = 0
           return
c
c     zero pivot :
c
 600       ierr = ii
c
           return
c------- end-of-ilu0 ---------------------------------------------------
c-----------------------------------------------------------------------
           end

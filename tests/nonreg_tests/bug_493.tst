// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- Non-regression test for bug 493 -->
//
// <-- Bugzilla URL -->
// http://forge.scilab.org/index.php/p/spilu/issues/493/
//
// <-- Short Description -->
//   The example of spilu_ilu0 failed on windows.


// <-- JVM NOT MANDATORY -->

path = spilu_getpath (  );
filename = fullfile(path,"tests","matrices","nos1.mtx");
A=mmread(filename);
n=size(A,1);
b=ones(n,1);
[L,U]=spilu_ilu0(A);

// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->


// Test error cases
A = [
1 2 0 0 0
3 4 5 0 0
0 6 7 8 0
];
//
instr = "[L,U]=spilu_ilu0M(A)";
name="spilu_ilu0M";
msg1="%s: Expected type [""sparse""] for input argument A at input #1, but got ""constant"" instead.";
assert_checkerror( instr, msg1, [], name);
//
A = sparse(A);
instr = "[L,U]=spilu_ilu0M(A)";
name="spilu_ilu0M";
msg1="%s: Expected a square matrix for input argument A at input #1, but got [3 5] instead.";
assert_checkerror( instr, msg1, [], name);
//
// Simplest possible test case
A = [
1 2 0
3 4 5
0 6 7
];
A = sparse(A);
[L,U]=spilu_ilu0M(A);
Le = [
1  0 0
3  1 0
0 -3 1
];
Le = sparse(Le);
Ue = [
1  2 0
0 -2 5
0  0 22
];
Ue = sparse(Ue);
assert_checkequal(L,Le);
assert_checkequal(U,Ue);
//
// Test case
A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
n=size(A,1);
A=sparse(A);
//default
expL=[
1.    0.    0.     0.           0.           0.
-2.    1.    0.     0.           0.           0.
0.    0.    1.     0.           0.           0.
0.    0.   -3.5    1.           0.           0.
3.    0.    0.     0.4210526    1.           0.
0.    5.    0.     0.           1.1666667    1.
];
expU=[
-1.    3.    0.    0.     4.    0.
0.    1.    0.    0.     0.    1.
0.    0.   -2.    3.     0.    0.
0.    0.    0.    9.5    0.    0.
0.    0.    0.    0.    -6.    0.
0.    0.    0.    0.     0.    3.
];
[L,U]=spilu_ilu0M(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkequal(U,expU);

// Zero pivot error
A=[
0.     3.    0.    0.    4.    0.
2.    -5.    0.    0.    0.    1.
0.     0.   -2.    3.    0.    0.
0.     0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.     5.    0.    0.   -7.    8.
];
A=sparse(A);
msg1="%s: zero pivot encountered at step number %d.";
instr = "[L,U]=spilu_ilu0M(A)";
assert_checkerror ( instr, msg1, [], "spilu_ilu0M",2);


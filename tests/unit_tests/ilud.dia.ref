// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
// <-- JVM NOT MANDATORY -->
//
// default calling sequence
A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
expL=[
1.    0.    0.     0.           0.           0.
-2.    1.    0.     0.           0.           0.
0.    0.    1.     0.           0.           0.
0.    0.   -3.5    1.           0.           0.
3.   -9.    0.     0.4210526    1.           0.
0.    5.    0.     0.          -0.7121212    1.
];
expU=[
-1.    3.    0.    0.     4.     0.
0.    1.    0.    0.     8.     1.
0.    0.   -2.    3.     0.     0.
0.    0.    0.    9.5    0.     0.
0.    0.    0.    0.     66.    9.
0.    0.    0.    0.     0.     9.4090909
];
[L,U]=spilu_ilud(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
//
// Configure alpha=1
A=[
    18.    0.    -6.     0.     3.
    0.     16.    0.     8.     0.
   -6.     0.     36.    0.     0.
    0.     8.     0.     16.    0.
    3.     0.     0.     0.     6.
];
A=sparse(A);
expL=[
    1.           0.     0.    0.    0.
    0.           1.     0.    0.    0.
    -0.3333333    0.     1.    0.    0.
    0.           0.5    0.    1.    0.
    0.1666667    0.     0.    0.    1.
];
expU=[
    18.    0.    -6.     0.     3.
    0.     16.    0.     8.     0.
    0.     0.     35.    0.     0.
    0.     0.     0.     12.    0.
    0.     0.     0.     0.     6.5
];
alpha=1;
[L,U]=spilu_ilud(A,alpha);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-6,"element");
assert_checkalmostequal(U,expU,0,1D-6,"element");
// Configure drop=1.
A=[
    18.    0.    -6.     0.     3.
    0.     16.    0.     8.     0.
   -6.     0.     36.    0.     0.
    0.     8.     0.     16.    0.
    3.     0.     0.     0.     6.
];
A=sparse(A);
expL=[
    1.    0.    0.    0.    0.
    0.    1.    0.    0.    0.
    0.    0.    1.    0.    0.
    0.    0.    0.    1.    0.
    0.    0.    0.    0.    1.
];
expU=[
    16.5    0.     0.     0.     0.
    0.      20.    0.     0.     0.
    0.      0.     33.    0.     0.
    0.      0.     0.     20.    0.
    0.      0.     0.     0.     7.5
];
alpha=0.5;
drop=1.;
[L,U]=spilu_ilud(A,alpha,drop);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-6,"element");
assert_checkalmostequal(U,expU,0,1D-6,"element");
//
// A 6-by-6 test case
// Zero row
// n = 6
// nnz = 15
A=[
0.     0.    0.    0.    0.    0.
2.    -5.    0.    0.    0.    1.
0.     0.   -2.    3.    0.    0.
0.     0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.     5.    0.    0.   -7.    8.
];
A=sparse(A);
instr = "[L,U]=spilu_ilud(A)";
msg1="%s: zero row encountered.";
assert_checkerror ( instr, msg1, [], "spilu_ilud");

// ====================================================================
// Copyright (C) 2011 -NII -Benoit Goepfert
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <--JVM NOT MANDATORY -->

// A simple 6-by-6 matrix
A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);

//default
expL=[
1.      0.      0.     0.           0.           0.
0.      1.      0.     0.           0.           0.
0.      0.      1.     0.           0.           0.
0.      0.     -3.5    1.           0.           0.
1.5     0.9     0.     0.4210526    1.           0.
-1.75   -2.05    0.     0.          -0.7121212    1.
];
expU=[
4.    3.    0.    0.    -1.     0.
0.   -5.    0.    0.     2.     1.
0.    0.   -2.    3.     0.     0.
0.    0.    0.    9.5    0.     0.
0.    0.    0.    0.    -3.3   -0.9
0.    0.    0.    0.     0.     9.4090909
];
expP=[5.    2.    3.    4.    1.    6.];
[L,U,perm]=spilu_iludp(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(expP,perm);

//alpha=0
alpha=0;
[L,U,perm]=spilu_iludp(A,alpha);
expL=[
    1.      0.      0.     0.           0.           0.
    0.      1.      0.     0.           0.           0.
    0.      0.      1.     0.           0.           0.
    0.      0.    -3.5    1.           0.           0.
    1.5     0.9     0.     0.4210526    1.           0.
  -1.75  -2.05    0.     0.         -0.7121212    1.
];
expU=[
    4.    3.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -3.3  -0.9
    0.    0.    0.    0.     0.     9.4090909
];
expPerm=[5.    2.    3.    4.    1.    6.];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=0.5
alpha=0.5;
[L,U,perm]=spilu_iludp(A,alpha);
expL=[
    1.      0.      0.     0.           0.           0.
    0.      1.      0.     0.           0.           0.
    0.      0.      1.     0.           0.           0.
    0.      0.    -3.5    1.           0.           0.
    1.5     0.9     0.     0.4210526    1.           0.
  -1.75  -2.05    0.     0.         -0.7121212    1.
];
expU=[
    4.    3.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -3.3  -0.9
    0.    0.    0.    0.     0.     9.4090909
];
expPerm=[5.    2.    3.    4.    1.    6. ];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=1
alpha=1;
[L,U,perm]=spilu_iludp(A,alpha);
expL=[
    1.      0.      0.     0.           0.           0.
    0.      1.      0.     0.           0.           0.
    0.      0.      1.     0.           0.           0.
    0.      0.    -3.5    1.           0.           0.
    1.5     0.9     0.     0.4210526    1.           0.
  -1.75  -2.05    0.     0.         -0.7121212    1.
];
expU=[
    4.    3.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -3.3  -0.9
    0.    0.    0.    0.     0.     9.4090909
];
expPerm=[5.    2.    3.    4.    1.    6. ];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=0.5 drop=0
drop=0;
alpha=0.5;
[L,U,perm]=spilu_iludp(A,alpha,drop);
expL=[
    1.      0.      0.     0.           0.           0.
    0.      1.      0.     0.           0.           0.
    0.      0.      1.     0.           0.           0.
    0.      0.    -3.5    1.           0.           0.
    1.5     0.9     0.     0.4210526    1.           0.
  -1.75  -2.05    0.     0.         -0.7121212    1.
];
expU=[
    4.    3.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -3.3  -0.9
    0.    0.    0.    0.     0.     9.4090909
];
expPerm=[5.    2.    3.    4.    1.    6. ];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=0.5 drop=0.2
drop=0.2;
alpha=0.5;
[L,U,perm]=spilu_iludp(A,alpha,drop);
expL=[
    1.      0.      0.     0.           0.           0.
    0.      1.      0.     0.           0.           0.
    0.      0.      1.     0.           0.           0.
    0.      0.    -3.5    1.           0.           0.
    1.5     0.9     0.     0.4210526    1.           0.
  -1.75  -2.05    0.     0.         -0.7121212    1.
];
expU=[
    4.    3.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -3.3  -0.9
    0.    0.    0.    0.     0.     9.4090909
];
expPerm=[5.    2.    3.    4.    1.    6. ];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=0.5 drop=0.5
drop=0.5;
alpha=0.5;
[L,U,perm]=spilu_iludp(A,alpha,drop);
expL=[
    1.      0.           0.     0.           0.    0.
    0.      1.           0.     0.           0.    0.
    0.      0.           1.     0.           0.    0.
    0.      0.         -3.5    1.           0.    0.
    1.5     1.           0.     0.4210526    1.    0.
  -1.75  -2.2777778    0.     0.           0.    1.
];
expU=[
    4.    3.     0.    0.   -1.     0.
    0.  -4.5    0.    0.     2.     0.
    0.    0.   -2.    3.     0.     0.
    0.    0.     0.    9.5    0.     0.
    0.    0.     0.    0.   -3.5    0.
    0.    0.     0.    0.     0.     9.4027778
];
expPerm=[5.    2.    3.    4.    1.    6.];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=0.5 drop=0.1 ptol=0
drop=0.1;
alpha=0.5;
ptol=0;
[L,U,perm]=spilu_iludp(A,alpha,drop,ptol);
expL=[
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
expPerm=[1.    2.    3.    4.    5.    6.];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=3 drop=0.1 ptol=1
drop=0.1;
alpha=0.5;
ptol=1;
[L,U,perm]=spilu_iludp(A,alpha,drop,ptol);
expL=[
    1.      0.      0.           0.           0.           0.
    0.      1.      0.           0.           0.           0.
    0.      0.      1.           0.           0.           0.
    0.      0.    -0.3333333    1.           0.           0.
    1.5     0.9     1.3333333    0.4210526    1.           0.
  -1.75  -2.05    0.           0.         -0.7121212    1.
];
expU=[
    4.    3.    0.    0.         -1.     0.
    0.  -5.    0.    0.           2.     1.
    0.    0.    3.  -2.           0.     0.
    0.    0.    0.    6.3333333    0.     0.
    0.    0.    0.    0.         -3.3  -0.9
    0.    0.    0.    0.           0.     9.4090909
];
expPerm=[5.    2.    4.    3.    1.    6.];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=3 drop=0.1 ptol=0.5 bloc=3
drop=0.1;
alpha=0.5;
ptol=0.5;
bloc=3;
[L,U,perm]=spilu_iludp(A,alpha,drop,ptol,bloc);
expL=[
    1.           0.    0.     0.           0.           0.
  -1.6666667    1.    0.     0.           0.           0.
    0.           0.    1.     0.           0.           0.
    0.           0.  -3.5    1.           0.           0.
    0.         -9.    0.     0.4210526    1.           0.
    1.6666667    5.    0.     0.         -0.7121212    1.
];
expU=[
    3.  -1.           0.    0.     4.           0.
    0.    0.3333333    0.    0.     6.6666667    1.
    0.    0.         -2.    3.     0.           0.
    0.    0.           0.    9.5    0.           0.
    0.    0.           0.    0.     66.          9.
    0.    0.           0.    0.     0.           9.4090909
];
expPerm=[2.    1.    3.    4.    5.    6.];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//alpha=3 drop=0.1 ptol=0.5 bloc=1
drop=0.1;
alpha=0.5;
ptol=0.5;
bloc=1;
[L,U,perm]=spilu_iludp(A,alpha,drop,ptol,bloc);
expL=[
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
expPerm=[1.    2.    3.    4.    5.    6.];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
assert_checkequal(perm,expPerm);

//bloc=0 (!!!CRASH!!!)

//zero pivot
A=[
-1.    3.    0.    0.    4.    0.
2.   0.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
[L,U,perm]=spilu_iludp(A);
expL=[
    1.      0.       0.     0.           0.           0.
    0.      1.       0.     0.           0.           0.
    0.      0.       1.     0.           0.           0.
    0.      0.     -3.5    1.           0.           0.
    1.5   -0.75     0.     0.4210526    1.           0.
  -1.75  -0.875    0.     0.         -2.2777778    1.
];
expU=[
    4.  -1.    0.    0.     3.     0.
    0.    2.    0.    0.     0.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -4.5    0.75
    0.    0.    0.    0.     0.     10.583333
];
expPerm=[5.    1.    3.    4.    2.    6.];
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,1.e-7,1D-7,"element");
assert_checkalmostequal(U,expU,1.e-7,1D-7,"element");
assert_checkequal(perm,expPerm);

//wrong matrix size (!!!CRASH!!!)
//B=[ -1.    3.    0.    0.    4.    0.
//   2.   -5.    0.    0.    0.    1.
//   0.    0.   -2.    3.    0.    0.
//     0.    0.    7.   -1.    0.    0.
//    -3.    0.    0.    4.    6.    0. ];
//spB=sparse(B);
//[L,U,perm]=spilu_iludp(spB);

// error : zero row encountered in A or U
A=[
-1.    3.    0.    0.    4.    0.
0.    0.    0.    0.    0.    0.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
msg_5="%s: zero row encountered.";
instr = "[L,U,perm]=spilu_iludp(A)";
assert_checkerror ( instr, msg_5, [], "spilu_iludp");

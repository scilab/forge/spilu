// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

// A small 3-by-3 matrix
// nnz(A)=7
A = [
1 2 0
3 4 5
0 6 7
];
A = sparse(A);
// Test all methods
for m = spilu_iluhubavail()
    mprintf("Method: %s\n",m);
    [L,U,perm]=spilu_iluhub(A,m);
    mprintf("    nnz(LU)= %d\n",nnz(L)+nnz(U));
end

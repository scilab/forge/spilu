// ====================================================================
// Copyright (C) 2011 -NII -Benoit Goepfert
// Copyright (C) 2011 -Digiteo -Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <--JVM NOT MANDATORY -->

A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);

//default
expL=[
1.    0.    0.     0.           0.           0.
-2.    1.    0.     0.           0.           0.
0.    0.    1.     0.           0.           0.
0.    0.   -3.5    1.           0.           0.
3.   -9.    0.     0.4210526    1.           0.
0.    5.    0.     0.          -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
[L,U]=spilu_iluk(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//lfil=0
lfil=0;
[L,U]=spilu_iluk(A,lfil);
expL=[
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
[L,U]=spilu_iluk(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//lfil=1
lfil=1;
[L,U]=spilu_iluk(A,lfil);
expL=[
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
[L,U]=spilu_iluk(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//lfil=2
lfil=2;
[L,U]=spilu_iluk(A,lfil);
expL=[
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
expU=[
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
[L,U]=spilu_iluk(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

//error : zero row encountered in A or U
A=[
0    0.    0.    0.    0.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
instr = "[L,U]=spilu_iluk(A)";
msg="%s: zero row encountered in A or U.";
assert_checkerror ( instr, msg, [], "spilu_iluk");

//error : illegal value for lfil
A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
lfil=-1;
msg="%s: Wrong value for input argument #2: Must be > 0.";
instr = "[L,U]=spilu_iluk(A,lfil)";
assert_checkerror ( instr, msg, [], "spilu_iluk");


// error : zero pivot encountered
A=[
0.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
instr = "[L,U]=spilu_iluk(A)";
msg="%s: zero row encountered in A or U.";
assert_checkerror ( instr, msg, [], "spilu_iluk");

// not enough memory for matrix L
A = [
    56.  -12.    3.     5.   -1.     7.
  -12.    24.    0.     0.     0.     0.
    3.     0.     24.    0.     9.     0.
    5.     0.     0.     10.    0.     0.
  -1.     0.     9.     0.     20.    0.
    7.     0.     0.     0.     0.     14.
];
A = sparse(A);
instr = "[L,U]=spilu_iluk(A)";
msg="%s: not enough memory for matrix L.";
assert_checkerror ( instr, msg, [], "spilu_iluk");

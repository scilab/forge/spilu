// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

//
// Simplest possible test 3-by-3 test case
A = [
1 2 0
3 4 5
0 6 7
];
A = sparse(A);
[L,U]=spilu_ilukM(A,3);
Le = [
1  0 0
3  1 0
0 -3 1
];
Ue = [
1  2 0
0 -2 5
0  0 22
];
Le = sparse(Le);
Ue = sparse(Ue);
assert_checkequal(L,Le);
assert_checkequal(U,Ue);
//
// A 6-by-6 matrix
//
A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
[L,U]=spilu_ilukM(A);
Lexp = [
    1.0000         0         0         0         0         0
   -2.0000    1.0000         0         0         0         0
         0         0    1.0000         0         0         0
         0         0   -3.5000    1.0000         0         0
    3.0000   -9.0000         0    0.4211    1.0000         0
         0    5.0000         0         0   -0.7121    1.0000
];
Uexp = [
   -1.0000    3.0000         0         0    4.0000         0
         0    1.0000         0         0    8.0000    1.0000
         0         0   -2.0000    3.0000         0         0
         0         0         0    9.5000         0         0
         0         0         0         0   66.0000    9.0000
         0         0         0         0         0    9.4091
];
assert_checkalmostequal(full(L),Lexp,[],1.e-4);
assert_checkalmostequal(full(U),Uexp,[],1.e-4);
//
// Configure level-of-fill
n = size(A,"r");
lfil=floor(1+nnz(A)/n);
[L,U]=spilu_ilukM(A,lfil);
assert_checkalmostequal(full(L),Lexp,[],1.e-4);
assert_checkalmostequal(full(U),Uexp,[],1.e-4);

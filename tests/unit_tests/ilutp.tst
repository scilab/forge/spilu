// ====================================================================
// Copyright (C) 2011 -NII -Benoit Goepfert
// Copyright (C) 2011 -Digiteo -Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <--JVM NOT MANDATORY -->

// A 6-by-6 sparse matrix.
// nnz(A)=16
A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
n=size(A,1);
A=sparse(A);
e=(1:n)';
ne=norm(e);
b=A*e;
v2=9.539392;

//default
expL=[
1.      0.      0.     0.           0.           0.
0.      1.      0.     0.           0.           0.
0.      0.      1.     0.           0.           0.
0.      0.     -3.5    1.           0.           0.
1.5     0.9     0.     0.4210526    1.           0.
-1.75   -2.05    0.     0.          -0.7121212    1.
];
expU=[
4.    3.    0.    0.    -1.     0.
0.   -5.    0.    0.     2.     1.
0.    0.   -2.    3.     0.     0.
0.    0.    0.    9.5    0.     0.
0.    0.    0.    0.    -3.3   -0.9
0.    0.    0.    0.     0.     9.4090909
];
expP=[
0.    0.    0.    0.    1.    0.
0.    1.    0.    0.    0.    0.
0.    0.    1.    0.    0.    0.
0.    0.    0.    1.    0.    0.
1.    0.    0.    0.    0.    0.
0.    0.    0.    0.    0.    1.
];
[L,U,perm]=spilu_ilutp(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");
P=spilu_permVecToMat(perm);
P=full(P);
assert_checkequal(expP,P);

//lfil=0 cause an unexpected overflow in the storage of U

//lfil=1
lfil=1;
[L,U,perm]=spilu_ilutp(A,lfil);
Lexp = [
    1.    0.    0.     0.    0.           0.
  -2.    1.    0.     0.    0.           0.
    0.    0.    1.     0.    0.           0.
    0.    0.  -3.5    1.    0.           0.
    0.    0.    0.   -4.    1.           0.
    0.    0.    0.     0.  -1.1666667    1.
];
Uexp = [
  -1.    0.    0.    0.    0.    0.
    0.  -5.    0.    0.    0.    0.
    0.    0.  -2.    0.    0.    0.
    0.    0.    0.  -1.    0.    0.
    0.    0.    0.    0.    6.    0.
    0.    0.    0.    0.    0.    8.
];
perm_exp = [1.    2.    3.    4.    5.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//lfil=2
lfil=2;
[L,U,perm]=spilu_ilutp(A,lfil);
Lexp = [
    1.      0.    0.     0.           0.    0.
    0.      1.    0.     0.           0.    0.
    0.      0.    1.     0.           0.    0.
    0.      0.  -3.5    1.           0.    0.
    1.5     0.    0.     0.4210526    1.    0.
  -1.75  -1.    0.     0.           0.    1.
];
Uexp = [
    4.    0.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     0.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -1.5    0.
    0.    0.    0.    0.     0.     8.
];
perm_exp = [5.    2.    3.    4.    1.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//lfil=3
lfil=3;
[L,U,perm]=spilu_ilutp(A,lfil);
Lexp = [
    1.      0.      0.     0.           0.           0.
    0.      1.      0.     0.           0.           0.
    0.      0.      1.     0.           0.           0.
    0.      0.    -3.5    1.           0.           0.
    1.5     0.9     0.     0.4210526    1.           0.
  -1.75  -2.05    0.     0.         -0.7121212    1.
];
Uexp = [
    4.    3.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -3.3  -0.9
    0.    0.    0.    0.     0.     9.4090909
];
perm_exp = [5.    2.    3.    4.    1.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//lfil=3 drop=0.5
drop=0.5;
lfil=3;
[L,U,perm]=spilu_ilutp(A,lfil,drop);
Lexp = [
    1.      0.      0.     0.    0.           0.
    0.      1.      0.     0.    0.           0.
    0.      0.      1.     0.    0.           0.
    0.      0.    -3.5    1.    0.           0.
    1.5     0.9     0.     0.    1.           0.
  -1.75  -2.05    0.     0.  -0.7121212    1.
];
Uexp = [
    4.    3.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     0.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -3.3    0.
    0.    0.    0.    0.     0.     8.
];
perm_exp = [5.    2.    3.    4.    1.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//lfil=3 drop=0.1
drop=0.1;
lfil=3;
[L,U,perm]=spilu_ilutp(A,lfil,drop);
Lexp = [
    1.      0.      0.     0.           0.           0.
    0.      1.      0.     0.           0.           0.
    0.      0.      1.     0.           0.           0.
    0.      0.    -3.5    1.           0.           0.
    1.5     0.9     0.     0.4210526    1.           0.
  -1.75  -2.05    0.     0.         -0.7121212    1.
];
Uexp = [
    4.    3.    0.    0.   -1.     0.
    0.  -5.    0.    0.     2.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.   -3.3  -0.9
    0.    0.    0.    0.     0.     9.4090909
];
perm_exp = [5.    2.    3.    4.    1.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//lfil=3 drop=0.1 ptol=0
drop=0.1;
lfil=3;
ptol=0;
[L,U,perm]=spilu_ilutp(A,lfil,drop,ptol);
Lexp = [
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
Uexp = [
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
perm_exp = [1.    2.    3.    4.    5.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//lfil=3 drop=0.1 ptol=1
drop=0.1;
lfil=3;
ptol=1;
[L,U,perm]=spilu_ilutp(A,lfil,drop,ptol);
Lexp = [
    1.      0.      0.           0.    0.           0.
    0.      1.      0.           0.    0.           0.
    0.      0.      1.           0.    0.           0.
    0.      0.    -0.3333333    1.    0.           0.
    1.5     0.9     1.3333333    0.    1.           0.
  -1.75  -2.05    0.           0.  -0.7121212    1.
];
Uexp = [
    4.    3.    0.    0.         -1.     0.
    0.  -5.    0.    0.           2.     1.
    0.    0.    3.  -2.           0.     0.
    0.    0.    0.    6.3333333    0.     0.
    0.    0.    0.    0.         -3.3  -0.9
    0.    0.    0.    0.           0.     9.4090909
];
perm_exp = [5.    2.    4.    3.    1.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//lfil=3 drop=0.1 ptol=0.5 bloc=3
drop=0.1;
lfil=3;
ptol=0.5;
bloc=3;
[L,U,perm]=spilu_ilutp(A,lfil,drop,ptol,bloc);
Lexp = [
    1.           0.    0.     0.           0.           0.
  -1.6666667    1.    0.     0.           0.           0.
    0.           0.    1.     0.           0.           0.
    0.           0.  -3.5    1.           0.           0.
    0.         -9.    0.     0.4210526    1.           0.
    1.6666667    5.    0.     0.         -0.7121212    1.
];
Uexp = [
    3.  -1.           0.    0.     4.           0.
    0.    0.3333333    0.    0.     6.6666667    1.
    0.    0.         -2.    3.     0.           0.
    0.    0.           0.    9.5    0.           0.
    0.    0.           0.    0.     66.          9.
    0.    0.           0.    0.     0.           9.4090909
];
perm_exp = [2.    1.    3.    4.    5.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//lfil=3 drop=0.1 ptol=0.5 bloc=1
drop=0.1;
lfil=3;
ptol=0.5;
bloc=1;
[L,U,perm]=spilu_ilutp(A,lfil,drop,ptol,bloc);
Lexp = [
    1.    0.    0.     0.           0.           0.
  -2.    1.    0.     0.           0.           0.
    0.    0.    1.     0.           0.           0.
    0.    0.  -3.5    1.           0.           0.
    3.  -9.    0.     0.4210526    1.           0.
    0.    5.    0.     0.         -0.7121212    1.
];
Uexp = [
  -1.    3.    0.    0.     4.     0.
    0.    1.    0.    0.     8.     1.
    0.    0.  -2.    3.     0.     0.
    0.    0.    0.    9.5    0.     0.
    0.    0.    0.    0.     66.    9.
    0.    0.    0.    0.     0.     9.4090909
];
perm_exp = [1.    2.    3.    4.    5.    6.];
assert_checkalmostequal(full(L),Lexp,1.e-6,1D-7,"element");
assert_checkalmostequal(full(U),Uexp,1.e-6,1D-7,"element");
assert_checkequal(perm,perm_exp);

//bloc=0 (!!!CRASH!!!)

//zero row
A=[
-1.   3.    0.    0.    4.    0.
0.    0.    0.    0.    0.    0.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.   0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A = sparse(A);
instr = "[L,U,perm]=spilu_ilutp(A)";
msg1="%s: zero row encountered.";
assert_checkerror ( instr, msg1, [], "spilu_ilutp");

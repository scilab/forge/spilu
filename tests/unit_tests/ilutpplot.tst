// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

A = [
30.    8.     7.     0.    0.
8.     18.    0.     0.   -1.
7.     0.     14.    0.    0.
0.     0.     0.     1.    0.
0.    -1.     0.     0.    2.
];
A = sparse(A);
//
spilu_ilutpplot(A);
delete(gcf());
delete(gcf());
delete(gcf());
delete(gcf());
//
spilu_ilutpplot(A,100);
delete(gcf());
delete(gcf());
delete(gcf());
delete(gcf());
//
data = spilu_ilutpplot(A,100);
assert_checkequal(size(data),[100,12]);
delete(gcf());
delete(gcf());
delete(gcf());
delete(gcf());
//
[data,hLfil,hDrop,hPermtol,hBloc] = spilu_ilutpplot(A,100);
delete(hLfil);
delete(hDrop);
delete(hPermtol);
delete(hBloc);

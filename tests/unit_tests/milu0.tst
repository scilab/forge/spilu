// ====================================================================
// Copyright (C) 2011 - NII - Benoit Goepfert
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

// A simple 6-by-6 matrix.
A=[
-1.    3.    0.    0.    4.    0.
2.   -5.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
//default
expL=[
1.    0.           0.     0.           0.           0.
-2.    1.           0.     0.           0.           0.
0.    0.           1.     0.           0.           0.
0.    0.          -3.5    1.           0.           0.
3.    0.           0.     0.4210526    1.           0.
0.    0.5555556    0.     0.           0.4666667    1.
];
expU=[
-1.    3.    0.    0.     4.     0.
0.    9.    0.    0.     0.     1.
0.    0.   -2.    3.     0.     0.
0.    0.    0.    9.5    0.     0.
0.    0.    0.    0.    -15.    0.
0.    0.    0.    0.     0.     7.4444444
];
[L,U]=spilu_milu0(A);
L=full(L);
U=full(U);
assert_checkalmostequal(L,expL,0,1D-7,"element");
assert_checkalmostequal(U,expU,0,1D-7,"element");

// Zero pivot
A=[
-1.    3.    0.    0.    4.    0.
2.     0.    0.    0.    0.    1.
0.    0.   -2.    3.    0.    0.
0.    0.    7.   -1.    0.    0.
-3.    0.    0.    4.    6.    0.
0.    5.    0.    0.   -7.    8.
];
A=sparse(A);
instr = "[L,U]=spilu_milu0(A)";
msg1="%s: zero pivot encountered at step number 2.";
assert_checkerror ( instr, msg1, [], "spilu_milu0");

// ====================================================================
// Copyright (C) 2011 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [pNorm, pNnz]=iludDecompAlpha(A, alpha)
instr = "[L,U]=spilu_ilud(A,alpha)";
ierr = execstr(instr,"errcatch");
if (ierr<>0) then
pNorm = %inf
pNnz = nnz(A)
else
pNorm = norm(A-L*U,"inf");
pNnz = nnz(L) + nnz(U)
end
endfunction
A = [
34.    4.   -10.    0.  -3.
4.     24.  -8.     0.    0.
-10.  -8.     36.    0.    0.
0.     0.     0.     1.    0.
-3.     0.     0.     0.    6.
];
A = sparse(A);
N = 100;
alpha = linspace(0,1,N)';
scf();
[pNorm,pNnz] = spilu_plotparameter(A,"alpha",alpha,..
    list(iludDecompAlpha),%f);
assert_checkequal(size(pNorm),[100 1]);
assert_checkequal(size(pNnz),[100 1]);
